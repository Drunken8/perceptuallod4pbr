#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Texture.h"
#include <cmath>
#include "cinder/Log.h"
#include "cinder/ObjLoader.h"
#include "cinder/gl/Fbo.h"
#include "cinder/Rand.h"
#include <ctime>
#include "cinder/Xml.h"

using namespace ci;
using namespace ci::app;
using namespace std;

struct PHI
{
	dvec4 toVec4() {
		return dvec4(r.x*alpha, r.y*alpha, r.z*alpha,alpha);
	}

	double kappa =5;
	double alpha =0;
	dvec3 mu = dvec3(0,0,0);
	dvec3 r = dvec3(0,0,0);
};

enum Mode
{
	VMF,
	MIP,
	SIMPLE,
	CTS,
	CTSVMF,
	SUPERSAMPLE,
	DEFERRED,
	DATAGEN
};

static std::unordered_map<std::string, Mode> const table = { {"VMF",Mode::VMF }, {"MIP",Mode::MIP},
	{"SIMPLE",Mode::SIMPLE}, {"CTS",Mode::CTS}, {"CTSVMF",Mode::CTSVMF}, {"SUPERSAMPLE",Mode::SUPERSAMPLE},
{"DEFERRED",Mode::DEFERRED}, {"DATAGEN",Mode::DATAGEN} };

class vMFFrameApp : public App {
	#define ENCODE(x,y,d,channels) (x*d*channels+y*channels)

  public:
	void setupVMF();
	void setupMIP();
	void setupSIMPLE();
	cinder::gl::TextureRef assembleMIPMAP( string preamble, string post);
	void setupCTSvMF(bool loadFromFile);
	void setupCTS();
	void setupDeferred(uint8_t startbind);
	void setupDataGen();

	void drawDeferred(cinder::gl::BatchRef toDraw, cinder::gl::GlslProgRef prog, unsigned int imageID);
	void drawSuperSampled(cinder::gl::BatchRef toDraw, cinder::gl::GlslProgRef prog, unsigned int imageID);
	void drawNormal(cinder::gl::BatchRef toDraw, cinder::gl::GlslProgRef prog);
	void setupScene(unsigned int microRandSeed);
	void drawDataGen(cinder::gl::BatchRef toDrawDeferred, cinder::gl::BatchRef toDrawSupersampled, cinder::gl::GlslProgRef progDeffered, cinder::gl::GlslProgRef progSupersampled);

	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void mouseDrag(MouseEvent event) override;
	void keyDown(KeyEvent event) override;
	void update() override;
	void draw() override;
	gl::TextureRef* makeLobeTextures(int noLobes, ImageSourceRef normals, Surface* SVMs, int noSVMs);

	double movMF(dvec3 n, PHI * phi, int count);

	double vMF(double nmu, double kappa);

	unsigned short ** create2DArray(int height, int width);

	gl::FboRef myMsaaFbo;
	gl::FboRef downsampled;
	unsigned int gEyeDirViewSpace, gNormalViewSpace, gTexCoords, gTangentViewSpace, gLightDirViewSpace;

	CameraPersp			mCam;
	gl::BatchRef		mSphere;
	gl::BatchRef		mSphereDeferred;
	gl::TextureRef		uDiffuseMap;
	gl::TextureRef		uDiffuseMapDeferred;
	gl::TextureRef		uNormalMap;
	gl::TextureRef		uMetalMap;
	gl::TextureRef		uRoughMap;
	gl::TextureRef		uAOMap;
	  
	gl::GlslProgRef		mGlsl;
	gl::GlslProgRef		mGlslDeferred;
	string				vertName = "shader\\shader.vert";
	string				fragName = "shader\\shader.frag";
	gl::TextureRef*		lobes;


	unsigned int gBuffer;

	Mode renderMode = Mode::CTSVMF;

	vec2 oPos;
	XmlTree doc;

	vec3 cPos = vec3(7, 0, 0);
	vec3 lightPos = vec3(0,1.5,0);

	vec4* lightPoses = new vec4[4]{ vec4(lightPos,1), vec4(0,0,0,1), vec4(0,0,0,1), vec4(0,0,0,1) };
	vec3* lightCols = new vec3[4]{ vec3(3,3,3), vec3(0,0,0), vec3(0,0,0), vec3(0,0,0) };
};


void APIENTRY
MessageCallback(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam)
{
	fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
		(type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
		type, severity, message);
	CI_LOG_E("GLERROR: " << (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "") << "type= " << type << " severity= " << severity << " msg= " <<message);
}


void vMFFrameApp::setupVMF() {

	vertName = "shader\\VMF - Kopie.vert";
	fragName = "shader\\VMF - Kopie.frag";
	//prepare shaders
	try
	{
		mGlsl = gl::GlslProg::create(loadAsset(vertName), loadAsset(fragName));
	}
	catch (const cinder::gl::GlslProgCompileExc& e)
	{
		CI_LOG_E("compile error: " << e.getShaderType() << " WHAT: " << e.what());
	}


	//load images
	auto col = loadImage(loadAsset(doc.getChild("settings/AlbedoTexture").getValue()));
	auto norm = loadImage(loadAsset(doc.getChild("settings/NormalTexture").getValue()));

	//make lobes and bind

	CI_LOG_I("Making LOBES");
	int noLobes = 4;
	gl::TextureRef* lobes = makeLobeTextures(noLobes, norm, new Surface(col), 1);

	CI_LOG_I("Loading normal lobes");
	for (int i = 0; i < noLobes; i++)
	{
		//write to disk
		std::string path = "vMFmap" + to_string(i + 1) + ".png";
		writeImage(path, lobes[i]->createSource());

		//bind to GPU
		lobes[i]->bind(i);
		mGlsl->uniform("vMFmap" + to_string(i + 1), i);
	}


	CI_LOG_I("Loading other lobes");
	for (int i = 0; i < noLobes; i++)
	{
		lobes[i + noLobes]->bind(i + noLobes);
		mGlsl->uniform("uDiffuseMap" + to_string(i + 1), i + noLobes);
	}


	CI_LOG_I("Binding uniforms");
	//other uniforms
}

void vMFFrameApp::setupMIP() {

	vertName = "shader\\shader.vert";
	fragName = "shader\\shader.frag";
	//prepare shaders
	try
	{
		mGlsl = gl::GlslProg::create(loadAsset(vertName), loadAsset(fragName));
	}
	catch (const cinder::gl::GlslProgCompileExc& e)
	{
		CI_LOG_E("compile error: " << e.getShaderType() << " WHAT: " << e.what());
	}


	//load images
	auto col = loadImage(loadAsset(doc.getChild("settings/AlbedoTexture").getValue()));
	auto norm = loadImage(loadAsset(doc.getChild("settings/NormalTexture").getValue()));
	uDiffuseMap = gl::Texture::create(col, gl::Texture::Format().mipmap());
	uNormalMap = gl::Texture::create(norm, gl::Texture::Format().mipmap());

	uDiffuseMap->bind(0);
	uNormalMap->bind(1);

	CI_LOG_I("Binding uniforms");
	//other uniforms
	mGlsl->uniform("LightPosition", lightPos);
	mGlsl->uniform("uDiffuseMap", 0);
	mGlsl->uniform("uNormalMap", 1);
}

void vMFFrameApp::setupSIMPLE() {

	vertName = "shader\\shader.vert";
	fragName = "shader\\shader.frag";
	//prepare shaders
	try
	{
		mGlsl = gl::GlslProg::create(loadAsset(vertName), loadAsset(fragName));
	}
	catch (const cinder::gl::GlslProgCompileExc& e)
	{
		CI_LOG_E("compile error: " << e.getShaderType() << " WHAT: " << e.what());
	}


	//load images
	auto col = loadImage(loadAsset(doc.getChild("settings/AlbedoTexture").getValue()));//loadAsset("steelplate1_col.jpg"));
	auto norm = loadImage(loadAsset(doc.getChild("settings/NormalTexxture").getValue()));//loadAsset("steelplate1_nrm.jpg"));
	uDiffuseMap = gl::Texture::create(col, gl::Texture::Format().mipmap(false));
	uNormalMap = gl::Texture::create(norm, gl::Texture::Format().mipmap(false));

	uDiffuseMap->bind(0);
	uNormalMap->bind(1);

	CI_LOG_I("Binding uniforms");
	//other uniforms
	mGlsl->uniform("uDiffuseMap", 0);
	mGlsl->uniform("uNormalMap", 1);
}

cinder::gl::TextureRef vMFFrameApp::assembleMIPMAP(string preamble, string post) {


	cinder::ImageSourceRef tmp = loadImage(loadAsset(preamble + to_string(0) + post));
	int dimensions = tmp->getHeight();
	int maxLevel = (int) log2(dimensions)+1;

	cinder::gl::Texture::Format f = cinder::gl::Texture::Format();
	f.enableMipmapping(true);
	f.setMagFilter(GL_LINEAR);
	f.setMinFilter(GL_LINEAR_MIPMAP_LINEAR);
	f.setMaxMipmapLevel(maxLevel - 1);
	cinder::gl::TextureRef r = cinder::gl::Texture2d::create(dimensions, dimensions, f);

	for (int lod = 0; lod < maxLevel; lod++)
	{
		//load image from disk
		cinder::ImageSourceRef tmp = loadImage(loadAsset(preamble + to_string(lod) + post));
		CI_LOG_I(lod);
		//update lod
		r->update(cinder::Surface(tmp), lod);
	}

	return r;
}

void vMFFrameApp::setupCTSvMF(bool loadFromFile)
{

	int noLobes = 4;
	vertName = "shader\\CTSvMF.vert";
	fragName = "shader\\FOURCTSvMF.frag";
	//prepare shaders
	try
	{
		mGlsl = gl::GlslProg::create(loadAsset(vertName), loadAsset(fragName));
	}
	catch (const cinder::gl::GlslProgCompileExc& e)
	{
		CI_LOG_E("compile error: " << e.getShaderType() << " WHAT: " << e.what());
	}

	//load images
	if (!loadFromFile) {
		auto col = loadImage(loadAsset(doc.getChild("settings/AlbedoTexture").getValue()));
		auto norm = loadImage(loadAsset(doc.getChild("settings/NormalTexture").getValue()));
		auto metal = Surface(loadImage(loadAsset(doc.getChild("settings/MetalTexture").getValue())));
		auto rough = Surface(loadImage(loadAsset(doc.getChild("settings/RoughnessTexture").getValue())));
		auto ao = Surface(loadImage(loadAsset(doc.getChild("settings/AmbientOcclusionTexture").getValue())));


		Surface combi = metal.clone();
		Surface::Iter outputIter = combi.getIter();
		Surface::ConstIter inter1 = rough.getIter();
		Surface::ConstIter inter2 = ao.getIter();

		while (outputIter.line() && inter1.line() && inter2.line()) {
			while (outputIter.pixel() && inter1.pixel() && inter2.pixel()) {
				outputIter.r() = outputIter.r();
				outputIter.g() = inter1.r();
				outputIter.b() = inter2.r();
			}
		}


		//make lobes and bind
		Surface* matMaps = new Surface[2]();
		matMaps[0] = Surface(col);
		matMaps[1] = combi;


		CI_LOG_I("Making LOBES");


		lobes = makeLobeTextures(noLobes, norm, matMaps, 2);
	}
	else {

		CI_LOG_I("LOADING LOBES");
		lobes = new cinder::gl::TextureRef[noLobes + noLobes * 2]();
		for (int i = 0; i < noLobes; i++)
		{
			lobes[i] = assembleMIPMAP("PrecomputedLobes\\LOBE"+ to_string(i) + "LOD", "lobe.png");
			lobes[i]->setWrap(GL_REPEAT, GL_REPEAT);
		}

		CI_LOG_I("Loading albedo");
		for (int i = 0; i < noLobes; i++)
		{
			lobes[i+noLobes] = assembleMIPMAP("PrecomputedLobes\\LOBE" + to_string(i) + "LOD", "albedo.png");
			lobes[i+noLobes]->setWrap(GL_REPEAT, GL_REPEAT);
		}

		CI_LOG_I("Loading material");
		for (int i = 0; i < noLobes; i++)
		{
			lobes[i + noLobes*2] = assembleMIPMAP("PrecomputedLobes\\LOBE" + to_string(i) + "LOD", "material.png");
			lobes[i + noLobes*2]->setWrap(GL_REPEAT, GL_REPEAT);
		}

	}
	CI_LOG_I("Loading normal lobes");
	for (int i = 0; i < noLobes; i++)
	{
		//bind to GPU
		lobes[i]->bind(i);
		mGlsl->uniform("TexNormal" + to_string(i), i);
	}

	CI_LOG_I("Loading other lobes");
	for (int i = 0; i < noLobes; i++)
	{
		lobes[i + noLobes]->bind(i + noLobes);
		mGlsl->uniform("TexAlbedo" + to_string(i), i + noLobes);
	}
	for (int i = 0; i < noLobes; i++)
	{
		lobes[i + 2*noLobes]->bind(i + 2*noLobes);
		mGlsl->uniform("TexMats" + to_string(i), i + 2*noLobes);
	}



	CI_LOG_I("Binding uniforms");


	//other uniforms

	glm::mat4 viewMatrix = mCam.getViewMatrix();
	vec4* viewLights = new vec4[4]{ viewMatrix*lightPoses[0], viewMatrix*lightPoses[1],viewMatrix* lightPoses[2],viewMatrix* lightPoses[3] };
	mGlsl->uniform("lightPositions", viewLights, 4);
	mGlsl->uniform("lightColors", lightCols, 4);

	CI_LOG_I("Finished CTS setup");

}

void vMFFrameApp::setupCTS()
{

	vertName = "shader\\CTS.vert";
	fragName = "shader\\FOURCTS.frag";
	//prepare shaders
	try
	{
		mGlsl = gl::GlslProg::create(loadAsset(vertName), loadAsset(fragName));
	}
	catch (const cinder::gl::GlslProgCompileExc& e)
	{
		CI_LOG_E("compile error: " << e.getShaderType() << " WHAT: " << e.what());
	}

	//load images
	auto col = loadImage(loadAsset(doc.getChild("settings/AlbedoTexture").getValue()));
	auto norm = loadImage(loadAsset(doc.getChild("settings/NormalTexture").getValue()));
	auto metal = loadImage(loadAsset(doc.getChild("settings/MetalTexture").getValue()));
	auto rough = loadImage(loadAsset(doc.getChild("settings/RoughnessTexture").getValue()));
	auto ao = loadImage(loadAsset(doc.getChild("settings/AmbientOcclusionTexture").getValue()));
	
	bool usemip = true;
	uDiffuseMap = gl::Texture2d::create(col,gl::Texture::Format().mipmap(usemip));
	uNormalMap = gl::Texture2d::create(norm, gl::Texture::Format().mipmap(usemip));
	uMetalMap = gl::Texture2d::create(metal, gl::Texture::Format().mipmap(usemip));
	uRoughMap = gl::Texture2d::create(rough, gl::Texture::Format().mipmap(usemip));
	uAOMap = gl::Texture2d::create(ao, gl::Texture::Format().mipmap(usemip));// , gl::Texture::Format().mipmap());

	uDiffuseMap->setWrap(GL_REPEAT, GL_REPEAT);
	uNormalMap->setWrap(GL_REPEAT, GL_REPEAT);
	uMetalMap->setWrap(GL_REPEAT, GL_REPEAT);
	uRoughMap->setWrap(GL_REPEAT, GL_REPEAT);
	uAOMap->setWrap(GL_REPEAT, GL_REPEAT);
	
	CI_LOG_I("Binding uniforms");

	uDiffuseMap->bind(0);
	mGlsl->uniform("TexAlbedo", 0);
	uNormalMap->bind(1);
	mGlsl->uniform("TexNormal", 1);
	uMetalMap->bind(2);
	mGlsl->uniform("TexMetallic", 2);
	uRoughMap->bind(3);
	mGlsl->uniform("TexRoughness", 3);
	uAOMap->bind(4);
	mGlsl->uniform("TexAo", 4);


	glm::mat4 viewMatrix = mCam.getViewMatrix();
	vec4* viewLights = new vec4[4]{ viewMatrix* lightPoses[0],viewMatrix* lightPoses[1], viewMatrix*lightPoses[2], viewMatrix*lightPoses[3] };
	mGlsl->uniform("lightPositions", viewLights, 4);

	//other uniforms
	mGlsl->uniform("lightColors", lightCols, 4);

}

void vMFFrameApp::setupDeferred(uint8_t startbind)
{
	vertName = "shader\\deferred.vert";
	fragName = "shader\\deferred.frag";
	//prepare shaders
	try
	{
		mGlslDeferred = gl::GlslProg::create(loadAsset(vertName), loadAsset(fragName));
	}
	catch (const cinder::gl::GlslProgCompileExc& e)
	{
		CI_LOG_E("compile error: " << e.getShaderType() << " WHAT: " << e.what());
	}
	mGlslDeferred->bind();
	//load images
	auto col = loadImage(loadAsset(doc.getChild("settings/AlbedoTexture").getValue()));
	uDiffuseMapDeferred = gl::Texture::create(col, gl::Texture::Format().mipmap(true));
	uDiffuseMapDeferred->bind(startbind);
	mGlslDeferred->uniform("TexAlbedo0", startbind);

	glGenFramebuffers(1, &gBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);

	// - eyeDir color buffer
	glGenTextures(1, &gEyeDirViewSpace);
	glBindTexture(GL_TEXTURE_2D, gEyeDirViewSpace);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, this->getWindowWidth(), this->getWindowHeight(), 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gEyeDirViewSpace, 0);

	// - normal color buffer
	glGenTextures(1, &gNormalViewSpace);
	glBindTexture(GL_TEXTURE_2D, gNormalViewSpace);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, this->getWindowWidth(), this->getWindowHeight(), 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormalViewSpace, 0);

	// - texcoord color buffer
	glGenTextures(1, &gTexCoords);
	glBindTexture(GL_TEXTURE_2D, gTexCoords);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, this->getWindowWidth(), this->getWindowHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gTexCoords, 0);

	// - tangent color buffer
	glGenTextures(1, &gTangentViewSpace);
	glBindTexture(GL_TEXTURE_2D, gTangentViewSpace);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, this->getWindowWidth(), this->getWindowHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, gTangentViewSpace, 0);

	// - lightdir color buffer
	glGenTextures(1, &gLightDirViewSpace);
	glBindTexture(GL_TEXTURE_2D, gLightDirViewSpace);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, this->getWindowWidth(), this->getWindowHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, gLightDirViewSpace, 0);

	// - tell OpenGL which color attachments we'll use (of this framebuffer) for rendering 
	unsigned int attachments[5] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2,GL_COLOR_ATTACHMENT3,GL_COLOR_ATTACHMENT4};
	glDrawBuffers(5, attachments);

	// create and attach depth buffer (renderbuffer)
	unsigned int rboDepth;
	glGenRenderbuffers(1, &rboDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, this->getWindowWidth(), this->getWindowHeight());
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);
	// finally check if framebuffer is complete
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer not complete!" << std::endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void vMFFrameApp::setupDataGen()
{
	//setup two render programs
	//setupSIMPLE();
	//setupDeferred(2);
	//setupCTSvMF(false);
	setupCTS();
	setupDeferred(5);
	CI_LOG_I("Datagen setup finished");
}

void vMFFrameApp::setup()
{
	doc = XmlTree(loadAsset("settings.xml"));
	for each (auto var in table)
	{
		if (var.first.compare(doc.getChild("settings/RenderMode").getValue()) == 0) {
			renderMode = var.second;
		}
	}

	//enable debug output
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(MessageCallback, 0);
	log::makeLogger<log::LoggerFile>("/tmp/logging/cinder.log", true);

	mCam.lookAt(cPos, vec3(0, 0.0, 0));
	mCam.setAspectRatio(1);

	ObjLoader loader(loadAsset(doc.getChild("settings/mesh").getValue()));
	cinder::TriMesh obj = cinder::TriMesh(loader);
	obj.recalculateTangents();
	cinder::TriMesh obj2 = cinder::TriMesh(loader);
	obj2.recalculateTangents();

	switch (renderMode)
	{
	case VMF:
		setupVMF();
		mSphere = gl::Batch::create(obj, mGlsl);
		break;
	case MIP:
		setupMIP();
		mSphere = gl::Batch::create(obj, mGlsl);
		break;
	case SIMPLE:
		setupSIMPLE();
		mSphere = gl::Batch::create(obj, mGlsl);
		break;
	case CTS:
		setupCTS();
		mSphere = gl::Batch::create(obj, mGlsl);
		break;
	case CTSVMF:
		setupCTSvMF(doc.getChild("settings/LoadLobesFromFile").getValue().compare("true") == 0);
		mSphere = gl::Batch::create(obj, mGlsl);
		break;
	case DEFERRED:
		setupDeferred(0);
		mSphereDeferred = gl::Batch::create(obj2, mGlslDeferred);
		break;
	case DATAGEN:
		setupDataGen();
		mSphere = gl::Batch::create(obj, mGlsl);
		mSphereDeferred = gl::Batch::create(obj2, mGlslDeferred);
		break;
	default:
		setupCTS();
		mSphere = gl::Batch::create(obj, mGlsl);
		break;
	}

	CI_LOG_I("Enabling buffers");
	gl::enableDepthWrite();
	gl::enableDepthRead();

	if (renderMode == Mode::SUPERSAMPLE || renderMode == Mode::DATAGEN)
	{
		mGlsl->bind();
		CI_LOG_I("Setting up FBO for Supersampling");
		gl::Fbo::Format msaaFormat;
		msaaFormat.samples(std::stoi(doc.getChild("settings/SupersampleMSAA").getValue())); // enable 16x MSAA
		int multiplyer = std::stoi(doc.getChild("settings/SupersampleMultiplyer").getValue());
		myMsaaFbo = gl::Fbo::create(512* multiplyer, 512* multiplyer, msaaFormat); //4*4*16 Samples -> 512 with 256 samples per pixel
	}

	downsampled = gl::Fbo::create(512, 512);

	setupScene(std::stoi(doc.getChild("settings/sceneSeed").getValue()));
	CI_LOG_I("setup complete");
}

void vMFFrameApp::mouseDown( MouseEvent event )
{
	vec2 oPos = event.getPos();
	if (event.isLeft())
	{
		cPos = cPos + 0.1f*mCam.getViewDirection();
	}
	else
	{
		if (length(cPos - 0.1f*mCam.getViewDirection()-vec3(0,0,0)) > 0.1f)
		{
			cPos = cPos - 0.1f*mCam.getViewDirection();
		}
	}
	CI_LOG_I("MADE LOD #" << cPos);
	mCam.lookAt(cPos, vec3(0, 0.0, 0));
}

void vMFFrameApp::keyDown(KeyEvent event) {
	mat4 rot;
	switch (event.getCode())
	{
	case KeyEvent::KEY_DOWN:
		rot = glm::rotate(0.1f, vec3(mCam.getInverseViewMatrix()*vec4(1, 0, 0, 0)));
		break;
	case KeyEvent::KEY_UP:
		rot = glm::rotate(-0.1f, vec3(mCam.getInverseViewMatrix()*vec4(1, 0, 0, 0)));
		break;
	case KeyEvent::KEY_LEFT:
		rot = glm::rotate(-0.1f, vec3(0, 1, 0));
		break;
	case KeyEvent::KEY_RIGHT:
		rot = glm::rotate(0.1f, vec3(0, 1, 0));
		break;
	case KeyEvent::KEY_r:
		try
		{
			mGlsl = gl::GlslProg::create(loadAsset(vertName), loadAsset(fragName));
		}
		catch (const cinder::gl::GlslProgCompileExc& e)
		{
			CI_LOG_E("compile error: " << e.getShaderType() << " WHAT: " << e.what());
		}
		mSphere->replaceGlslProg(mGlsl);
		break;
	default:
		break;
	}


	cPos = ( rot*vec4(cPos, 0));
	mCam.lookAt(cPos, vec3(0, 0.0, 0));
}

void vMFFrameApp::mouseDrag(MouseEvent event) {
	
}

void vMFFrameApp::update()
{
}


void vMFFrameApp::drawDeferred(cinder::gl::BatchRef toDraw, cinder::gl::GlslProgRef prog, unsigned int imageID)
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	prog->bind();
	//https://learnopengl.com/Advanced-Lighting/Deferred-Shading
	//CI_LOG_I("Enabling buffers");
	gl::ScopedFaceCulling(true);
	gl::enableDepthWrite();
	gl::enableDepthRead();
	gl::enable(GL_DEPTH_TEST);
	gl::setMatrices(mCam);
	glm::mat4 viewMatrix = gl::getViewMatrix();
	vec4* viewLights = new vec4[4]{ viewMatrix*lightPoses[0], viewMatrix*lightPoses[1], viewMatrix*lightPoses[2], viewMatrix*lightPoses[3] };
	prog->uniform("lightPositions", viewLights, 4);

	toDraw->draw();
	//unbind framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	string setname = doc.getChild("settings/DatasetName").getValue();

	writeImage(setname+"/" + to_string(imageID) + "eyeDirViewSpace.png", cinder::gl::Texture2d::create(GL_TEXTURE_2D, gEyeDirViewSpace, this->getWindowWidth(), this->getWindowHeight(), true)->createSource());
	writeImage(setname + "/" + to_string(imageID) + "normalViewSpace.png", cinder::gl::Texture2d::create(GL_TEXTURE_2D, gNormalViewSpace, this->getWindowWidth(), this->getWindowHeight(), true)->createSource());
	writeImage(setname + "/" + to_string(imageID) + "texCoords.png", cinder::gl::Texture2d::create(GL_TEXTURE_2D, gTexCoords, this->getWindowWidth(), this->getWindowHeight(), true)->createSource());
	writeImage(setname + "/" + to_string(imageID) + "lightDirViewSpace.png", cinder::gl::Texture2d::create(GL_TEXTURE_2D, gLightDirViewSpace, this->getWindowWidth(), this->getWindowHeight(), true)->createSource());
	writeImage(setname + "/" + to_string(imageID) + "tangentViewSpace.png", cinder::gl::Texture2d::create(GL_TEXTURE_2D, gTangentViewSpace, this->getWindowWidth(), this->getWindowHeight(), true)->createSource());

}

void vMFFrameApp::drawSuperSampled(cinder::gl::BatchRef toDraw, cinder::gl::GlslProgRef prog, unsigned int imageID)
{
	prog->bind();
	gl::pushViewport();
	gl::viewport(ivec2(myMsaaFbo->getBounds().getWidth(), myMsaaFbo->getBounds().getHeight())); //set viewport s.t. opengl optimization draws bigger than window res
	//CI_LOG_I("Enabling buffers");
	gl::enableDepthWrite();
	gl::enableDepthRead();
	myMsaaFbo->bindFramebuffer();//render all to offscreen high-res fbo
	gl::clear(Color(0.0f, 0.0f, 0.0f));

	gl::setMatrices(mCam);
	glm::mat4 viewMatrix = gl::getViewMatrix();
	vec4* viewLights = new vec4[4]{ viewMatrix*lightPoses[0], viewMatrix*lightPoses[1], viewMatrix*lightPoses[2], viewMatrix*lightPoses[3] };
	prog->uniform("lightPositions", viewLights, 4);

	toDraw->draw();

	myMsaaFbo->unbindFramebuffer();//unbind s.t. later calls draw to screen fbo
	gl::popViewport();

	myMsaaFbo->resolveTextures(); //resolve multisampled texture to blitTo non multisampled fbo
	myMsaaFbo->blitTo(downsampled, myMsaaFbo->getBounds(), downsampled->getBounds(), GL_LINEAR, GL_COLOR_BUFFER_BIT); // downsample
	downsampled->blitToScreen(downsampled->getBounds(), this->getWindowBounds()); //show downsampled image on screen
	writeImage(doc.getChild("settings/DatasetName").getValue() +"/" + to_string(imageID) + "groundTruth.png", downsampled->getColorTexture()->createSource()); //save it


}

void vMFFrameApp::drawNormal(cinder::gl::BatchRef toDraw, cinder::gl::GlslProgRef prog)
{

	prog->bind();
	gl::pushViewport();
	gl::viewport(ivec2(downsampled->getBounds().getWidth(), downsampled->getBounds().getHeight())); //set viewport s.t. opengl optimization draws bigger than window res
	//CI_LOG_I("Enabling buffers");
	gl::enableDepthWrite();
	gl::enableDepthRead();
	downsampled->bindFramebuffer();//render all to offscreen high-res fbo
	gl::clear(Color(0.0f, 0.0f, 0.0f));

	gl::setMatrices(mCam);
	glm::mat4 viewMatrix = gl::getViewMatrix();
	vec4* viewLights = new vec4[4]{ viewMatrix*lightPoses[0], viewMatrix*lightPoses[1], viewMatrix*lightPoses[2], viewMatrix*lightPoses[3] };
	prog->uniform("lightPositions", viewLights, 4);

	toDraw->draw();

	downsampled->unbindFramebuffer();//unbind s.t. later calls draw to screen fbo
	gl::popViewport();

	downsampled->resolveTextures(); //resolve multisampled texture to blitTo non multisampled fbo
	downsampled->blitToScreen(downsampled->getBounds(), downsampled->getBounds(), GL_LINEAR, GL_COLOR_BUFFER_BIT);
	//writeImage("LQ.png", this->copyWindowSurface()); //save it

}

void vMFFrameApp::setupScene(unsigned int microRandSeed) {

	float maxViewDistance = std::stof(doc.getChild("settings/maxViewDistance").getValue());
	float minViewDistance = std::stof(doc.getChild("settings/minViewDistance").getValue());
	float maxLightDistance = std::stof(doc.getChild("settings/maxLightDistance").getValue());
	float minLightDistance = std::stof(doc.getChild("settings/minLightDistance").getValue());
	float viewDistance = -1;
	//init for this sample
	Rand microRand = Rand(microRandSeed);
	//DO-while lowres sum is not bright enough
	do
	{

		//generate randoms
		vec3 viewDir = microRand.nextVec3();
		viewDistance = minViewDistance + microRand.nextGaussian()*(1.0f / 3.0f)*(maxViewDistance - minViewDistance);
		vec3 lightDir = microRand.nextVec3();
		float lightDistance = minLightDistance + microRand.nextFloat()*(maxLightDistance - minLightDistance);

		//compute positions for camera and shaders
		vec3 viewPos = -viewDir * viewDistance;
		vec3 lightPos = -lightDir * lightDistance;

		//setup camera and shaders
		mCam.lookAt(viewPos, vec3(0, 0.0, 0)); //look at origin from position
		this->lightPoses[0] = vec4(lightPos, 1);
		this->cPos = viewPos;

		//TODO: Render in lowres

	} while (viewDistance < minViewDistance); //TODO: check if image good enough

}

void vMFFrameApp::drawDataGen(cinder::gl::BatchRef toDrawDeferred, cinder::gl::BatchRef toDrawSupersampled, cinder::gl::GlslProgRef progDeffered, cinder::gl::GlslProgRef progSupersampled)
{
	//prepare data gen
	Rand mainRand = Rand(std::stoi(doc.getChild("settings/DatasetSeed").getValue())); //master Seed 181Dataset 42evalset

	for (size_t i = 0; i < std::stoi(doc.getChild("settings/DataSetSize").getValue()); i++)
	{
		if (i % 100 == 0) {
			CI_LOG_I(i);
		}
		unsigned int microRandSeed = mainRand.nextUint();
		setupScene(microRandSeed);

		//render once with deferred
		drawDeferred(toDrawDeferred, progDeffered, microRandSeed);
		//drawNormal(toDrawSupersampled,progSupersampled);
		//writeImage("StatSet/" + to_string(microRandSeed) + "standard.png", this->copyWindowSurface());
		//render once for supersampled
		drawSuperSampled(toDrawSupersampled, progSupersampled, microRandSeed);

	}
	exit(0);

}


void vMFFrameApp::draw()
{
	switch (renderMode)
	{
	case SUPERSAMPLE:
		drawSuperSampled(mSphere, mGlsl,0);
		break;
	case DEFERRED:
		drawDeferred(mSphereDeferred, mGlslDeferred,0);
		break;
	case DATAGEN:
		drawDataGen(mSphereDeferred, mSphere, mGlslDeferred, mGlsl);
		break;
	default:
		drawNormal(mSphere, mGlsl);
		break;
	}
}



gl::TextureRef* vMFFrameApp::makeLobeTextures(int noLobes, ImageSourceRef normals, Surface* SVMs, int noSVMs)
{
	gl::TextureRef* targets = new gl::TextureRef[noLobes+noLobes*noSVMs];
	int globDim = normals->getHeight();
	int globLod = log2(globDim)+1;
	int dim = globDim;
	int sampleSize = 1;

	const double shift = 65535;

	//prepare normals for lookup
	Surface normalSurface = Surface(normals);
	GLuint* texture = new GLuint[noLobes+noSVMs*noLobes];
	unsigned short*** alobes = new unsigned short**[globLod];
	unsigned short**** aSVMs = new unsigned short***[noSVMs];
	for (int i = 0; i < noSVMs; i++)
	{
		aSVMs[i] = new unsigned short**[globLod];
	}


	for (int lod = 0; lod < globLod; lod++) //each LoD
	{
		CI_LOG_I("LOD#" << lod);
		alobes[lod] = create2DArray(noLobes, 4 * dim*dim);
		for (int i = 0; i < noSVMs; i++)
		{
			aSVMs[i][lod] = create2DArray(noLobes, 3 * dim*dim);
		}

		for (int y = 0; y < dim; y++) //each texel in x
		{
			for (int x = 0; x < dim; x++)//...and y
			{
				PHI* texelLobes = new PHI[noLobes];
				map<int, double>* z = new map<int, double>[noLobes];

				if (lod == 0) //init
				{ //lobe matches normal resolution, simple init
					for (int i = 0; i < noLobes; i++)
					{
						texelLobes[i].alpha = 1;
						texelLobes[i].r = normalize(dvec3(normalSurface.getPixel(glm::ivec2(y,dim-x)).get(cinder::ColorModel::CM_RGB)) * 2.0 -1.0)*0.99;

						while (length(texelLobes[i].r) > 1.0) {
							texelLobes[i].r *= 0.999;
						}
						texelLobes[i].mu = texelLobes[i].r;
						texelLobes[i].kappa = 128;
					}
					
				}
				else
				{ // complex init
					

					for (int lob = 0; lob < noLobes; lob++) //we have to init each lobe -> noLobes many centers
					{
						//check each possible candidate (even already used ones and from other lobes)

						float maxDist = -1;
						for (int d = 0; d < noLobes; d++) //all lobes per texel on this level
						{
							for (int j = y * 2; j < y * 2 + 2; j++)
							{
								for (int i = x * 2; i < x * 2 + 2; i++) //2x2 lower texels
								{
									//search maximal distance to all
									//current canditate
									double alph = alobes[lod - 1][d][ENCODE(i, j, dim * 2, 4) + 3] / shift;
									dvec3 c = normalize(dvec3(alobes[lod - 1][d][ENCODE(i, j, dim * 2, 4)] / shift * 2.0f - 1.0f, alobes[lod - 1][d][ENCODE(i, j, dim * 2, 4) + 1] / shift * 2.0f - 1.0f, alobes[lod - 1][d][ENCODE(i, j, dim * 2, 4) + 2] / shift * 2.0f - 1.0f)/alph);
									double distsum = 0;
									for (int p = 0; p < lob; p++)
									{
										distsum += length(c - texelLobes[p].mu);
									}

									if (distsum > maxDist) {
										//found better candiate
										maxDist = distsum;
										texelLobes[lob].alpha = alobes[lod - 1][d][ENCODE(i,j, dim * 2, 4) + 3] / shift;
										texelLobes[lob].r = dvec3(alobes[lod - 1][d][ENCODE(i,j, dim * 2, 4)] / shift, alobes[lod - 1][d][ENCODE(i,j, dim * 2, 4) + 1] / shift, alobes[lod - 1][d][ENCODE(i,j, dim * 2, 4) + 2] / shift) *2.0 - 1.0;
										texelLobes[lob].r = texelLobes[lob].r / texelLobes[lob].alpha;
										double r = length(texelLobes[lob].r);

										while (r > 1.0) {
											texelLobes[lob].r *= 0.999;
											r = length(texelLobes[lob].r);
										}
										texelLobes[lob].mu = normalize(texelLobes[lob].r);
										texelLobes[lob].kappa = (3.0*r - r * r*r) / (1.0 - r * r); //reconstruct kappa

									}
								}
							}
						}
						//CI_LOG_I("Settled on MU " << texelLobes[lob].mu << " FOR LOB " << lob);
					}
					
				}
				int counter = 0;
				dvec4 old = vec4(1,0,0,0);
				while ( counter !=20) //calculate pixel
				{
					old = texelLobes[0].toVec4();
					counter++;
					for (int i = 0; i < noLobes; i++)
					{
						z[i].clear();
					}
					//estep
					for (int j = y* sampleSize; j < y*sampleSize+ sampleSize; j++)
					{
						for (int i = x* sampleSize; i < x*sampleSize+ sampleSize; i++) //samples get larger with texel covering more texture in coarser lod
						{

							for (int lob = 0; lob < noLobes; lob++) //each lobe
							{
								const dvec3 cNormal = normalize(dvec3(normalSurface.getPixel(glm::ivec2(j, globDim - i)).get(cinder::ColorModel::CM_RGB))  *2.0 - 1.0)*0.99;
								double div = 0;
								for (int k = 0; k < noLobes; k++)								
								{
									div += vMF(dot(texelLobes[k].mu, cNormal), texelLobes[k].kappa);
								}
								double top = vMF(dot(texelLobes[lob].mu, cNormal), texelLobes[lob].kappa);

								//if (isnan(top / div) || isinf(top / div)) {
								//	goto optimization; //optimazation is already good enough
								//}

								z[lob][ENCODE(i,j,globDim,1)] =  top / div;
							}
						}
					}
					//mstep
					for (int lob = 0; lob < noLobes; lob++) //each lobe
					{
						double sumZ = 0;
						dvec3 sumZN = dvec3(0,0,0);
						long sum = 0;
						for each (auto sample in z[lob])
						{
							sum++;
							sumZ += sample.second;
							int ycoord = sample.first % globDim;
							int xcoord = (sample.first- ycoord) / globDim;
							sumZN += sample.second*normalize(dvec3(normalSurface.getPixel(glm::ivec2(ycoord, (globDim - xcoord))).get(cinder::ColorModel::CM_RGB)) *2.0 - 1.0)*0.99;
						}

						texelLobes[lob].alpha = sumZ / sum;
						texelLobes[lob].r = sumZN / sumZ;
						double r = length(texelLobes[lob].r);
						while(r > 1.0){
							texelLobes[lob].r *= 0.999;
							r = length(texelLobes[lob].r);
						}

						texelLobes[lob].kappa = (3 * r - r * r*r) / (1 - r * r);


						texelLobes[lob].mu = normalize(texelLobes[lob].r );
					}
				}

				//postprocess other spatially varying maps
				for (int lob = 0; lob < noLobes; lob++) //each lobe
				{
					double sumZ = 0;
					dvec3* sumSVMs = new dvec3[noSVMs];
					for (int i = 0; i < noSVMs; i++)
					{
						sumSVMs[i] = vec3(0, 0, 0);
					}
					for each (auto sample in z[lob])
					{
						sumZ += sample.second;
						int ycoord = sample.first % globDim;
						int xcoord = (sample.first - ycoord) / globDim;
						for (int i = 0; i < noSVMs; i++)
						{
							sumSVMs[i] += (float)sample.second*SVMs[i].getPixel(glm::ivec2(ycoord, (globDim - xcoord))).get(cinder::ColorModel::CM_RGB);
						}
					}

					for (int i = 0; i < noSVMs; i++)
					{
						sumSVMs[i] /= sumZ;
						aSVMs[i][lod][lob][ENCODE(x, y, dim, 3)] = sumSVMs[i].x*shift;
						aSVMs[i][lod][lob][ENCODE(x, y, dim, 3)+1] = sumSVMs[i].y*shift;
						aSVMs[i][lod][lob][ENCODE(x, y, dim, 3)+2] = sumSVMs[i].z*shift;
					}
				}

				//set w = alpha  = mu
				for (int lob = 0; lob < noLobes; lob++)
				{
					alobes[lod][lob][ENCODE(x, y, dim, 4)] = (texelLobes[lob].r.x*texelLobes[lob].alpha +1.0) / 2.0 * shift;
					alobes[lod][lob][ENCODE(x, y, dim, 4) + 1] = (texelLobes[lob].r.y*texelLobes[lob].alpha + 1.0) / 2.0 * shift;
					alobes[lod][lob][ENCODE(x, y, dim, 4) + 2] = (texelLobes[lob].r.z*texelLobes[lob].alpha + 1.0) / 2.0 * shift;
					alobes[lod][lob][ENCODE(x, y, dim, 4) + 3] = texelLobes[lob].alpha * shift;
				}
			}
		}

		CI_LOG_I("MADE LOD #" << lod);
		dim = dim / 2; //decrase size of texture
		sampleSize = sampleSize * 2; //increase size of sample area
	}

	//assembe final images
	CI_LOG_I("Assembling images");
	for (int lob = 0; lob < noLobes; lob++)
	{
		glGenTextures(1, &texture[lob]);
		glBindTexture(GL_TEXTURE_2D, texture[lob]);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, globLod-1);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, globLod - 1);


		for (int lod = 0; lod < globLod; lod++)
		{
			int cdim = globDim / powf(2, lod);
			glTexImage2D(GL_TEXTURE_2D, lod, GL_RGBA, cdim, cdim, 0, GL_RGBA, GL_UNSIGNED_SHORT, alobes[lod][lob]);
		}

		targets[lob] = cinder::gl::Texture2d::create(GL_TEXTURE_2D, texture[lob], globDim, globDim, false);
	}


	CI_LOG_I("Assembling other images");
	//assemble other images
	for (int i = 0; i < noSVMs; i++)
	{
		for (int lob = 0; lob < noLobes; lob++)
		{
			glGenTextures(1, &texture[noLobes + ENCODE(i, lob, noLobes, 1)]);
			glBindTexture(GL_TEXTURE_2D, texture[noLobes + ENCODE(i, lob, noLobes, 1)]);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, 0);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, globLod - 1);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, globLod - 1);

			for (int lod = 0; lod < globLod; lod++)
			{
				int cdim = globDim / powf(2, lod);
				glTexImage2D(GL_TEXTURE_2D, lod, GL_RGB, cdim, cdim, 0, GL_RGB, GL_UNSIGNED_SHORT, aSVMs[i][lod][lob]);
			}
			targets[noLobes+ENCODE(i,lob, noLobes,1)] = cinder::gl::Texture2d::create(GL_TEXTURE_2D, texture[noLobes + ENCODE(i, lob, noLobes, 1)], globDim, globDim, false);
		}
	}
	
	
	CI_LOG_I("Making LOD images");
	//make LOD images for lobes
	for (int lob = 0; lob < noLobes; lob++) {
		for (int i = 0; i < globLod; i++)
		{
			GLuint tmp;
			glGenTextures(1, &tmp);
			glBindTexture(GL_TEXTURE_2D, tmp);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
			

			int cdim = globDim / powf(2, i);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16, cdim, cdim, 0, GL_RGBA, GL_UNSIGNED_SHORT, alobes[i][lob]);

			writeImage("Images/LOBE" + to_string(lob)+"LOD" + to_string(i) + "lobe.png", cinder::gl::Texture2d::create(GL_TEXTURE_2D, tmp, cdim, cdim, false)->createSource());
		}
	}


	CI_LOG_I("Making SVM images");
	for (int svm = 0; svm < noSVMs; svm++) {
		for (int lob = 0; lob < noLobes; lob++) {
			for (int i = 0; i < globLod; i++)
			{
				GLuint tmp;
				glGenTextures(1, &tmp);
				glBindTexture(GL_TEXTURE_2D, tmp);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

				int cdim = globDim / powf(2, i);
				string svmType;

				switch (svm)
				{
				case 0:
					svmType = "albedo";
					break;
				case 1:
					svmType = "material";
					break;
				default:
					svmType = to_string(svm);
					break;
				}
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16, cdim, cdim, 0, GL_RGB, GL_UNSIGNED_SHORT, aSVMs[svm][i][lob]);

				writeImage("Images/LOBE" + to_string(lob) + "LOD" + to_string(i) + svmType + ".png", cinder::gl::Texture2d::create(GL_TEXTURE_2D, tmp, cdim, cdim, false)->createSource());
			}
		}
	}

	CI_LOG_I("returning LOBES");
	return targets;
}


double vMFFrameApp::movMF(dvec3 n, PHI* phi, int count) {
	double res = 0;
	for (int i = 0; i < count; i++)
	{
		res += phi[i].alpha*vMF(dot(n,phi[i].mu), phi[i].kappa);
	}
	return res;
}

inline double vMFFrameApp::vMF(double nmu, double kappa) {
	return kappa / (4 * M_PI*sinh(kappa))*exp(kappa*nmu);
}

unsigned short** vMFFrameApp::create2DArray(int height, int width)
{
	unsigned short** array2D = 0;
	array2D = new unsigned short*[height];

	for (int h = 0; h < height; h++)
	{
		array2D[h] = new unsigned short[width];

		for (int w = 0; w < width; w++)
		{
			// fill in some initial values
			array2D[h][w] = 0;
		}
	}

	return array2D;
}

CINDER_APP( vMFFrameApp, RendererGl(cinder::app::RendererGl::Options().msaa(0)), [&](App::Settings *settings) {
	settings->setWindowSize(512, 512);
	settings->setTitle("vMF Framework");
})
