#version 330 core
out vec4 FragColor;
in vec2 TexCoords;
in vec3 VertexViewSpace;
in vec3 NormalViewSpace;
in vec3 TangentViewSpace;
in vec3 BitangentViewSpace;

uniform sampler2D TexNormal0;
uniform sampler2D TexNormal1;
uniform sampler2D TexNormal2;
uniform sampler2D TexNormal3;
uniform sampler2D TexNormal4;
uniform sampler2D TexNormal5;

// material parameters
uniform sampler2D TexAlbedo0;
uniform sampler2D TexAlbedo1;
uniform sampler2D TexAlbedo2;
uniform sampler2D TexAlbedo3;
uniform sampler2D TexAlbedo4;
uniform sampler2D TexAlbedo5;
uniform sampler2D TexMats0; //r is metal, g is roughness, b is AO
uniform sampler2D TexMats1;
uniform sampler2D TexMats2;
uniform sampler2D TexMats3;
uniform sampler2D TexMats4;
uniform sampler2D TexMats5;

// lights
uniform vec3 lightColors[4];
uniform vec4 lightPositions[4];

const float PI = 3.14159265359;
const float gamma = 2.2;
const float exposure = 1.0;
const int noLobes = 6;
  
float DistributionGGX(vec3 N, vec3 H, float roughness);
float GeometrySchlickGGX(float NdotV, float roughness);
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness);
vec3 fresnelSchlick(float cosTheta, vec3 F0);

void main()
{		

    vec4 coeffs[6];
    vec3 colors[6];
    vec3 mat[6];

	coeffs[0] = texture2D(TexNormal0, TexCoords);
	coeffs[1] = texture2D(TexNormal1, TexCoords);
	coeffs[2] = texture2D(TexNormal2, TexCoords);
	coeffs[3] = texture2D(TexNormal3, TexCoords);
	coeffs[4] = texture2D(TexNormal4, TexCoords);
	coeffs[5] = texture2D(TexNormal5, TexCoords);

	colors[0] = texture2D(TexAlbedo0, TexCoords).xyz;//, vec3(gamma));
	colors[1] = texture2D(TexAlbedo1, TexCoords).xyz;//, vec3(gamma));
	colors[2] = texture2D(TexAlbedo2, TexCoords).xyz;//, vec3(gamma));
	colors[3] = texture2D(TexAlbedo3, TexCoords).xyz;//, vec3(gamma));
	colors[4] = texture2D(TexAlbedo4, TexCoords).xyz;//, vec3(gamma));
	colors[5] = texture2D(TexAlbedo5, TexCoords).xyz;//, vec3(gamma));

	mat[0] = texture2D(TexMats0, TexCoords).xyz;
	mat[1] = texture2D(TexMats1, TexCoords).xyz;
	mat[2] = texture2D(TexMats2, TexCoords).xyz;
	mat[3] = texture2D(TexMats3, TexCoords).xyz;
	mat[4] = texture2D(TexMats4, TexCoords).xyz;
	mat[5] = texture2D(TexMats5, TexCoords).xyz;

    vec3 color = vec3(0);
    vec3 V = normalize(-VertexViewSpace.xyz);
    
    for (int i=0; i<noLobes; i++) {
        //prepare lobe
        vec3 mu = coeffs[i].xyz*2.0-1.0;//map lobe from color space to real space
        float alpha = coeffs[i].w;
        mu = mu /  alpha;
        float r = length(mu);
        mu = normalize(mu); 
        float kappa = (3.0*r - r*r*r)/(1.0 - r*r);

        //prepare material parameters
        vec3 Normal = mu;
        vec3 albedo = colors[i];//, vec3(gamma))*alpha; //map color to linear space and apply scaling with alpha
        float metallic = mat[i].r;
        float roughness = sqrt(mat[i].g*mat[i].g+1/(2*kappa)); //scale roughness to fit lobes
        float ao = mat[i].b;

        vec3 N = normalize((TangentViewSpace * Normal.x) + (BitangentViewSpace * Normal.y) + (NormalViewSpace * Normal.z));
       
        vec3 F0 = vec3(0.04); 
        F0 = mix(F0, albedo, metallic);

        // reflectance equation
        vec3 Lo = vec3(0.0);
        for(int li = 0; li < 4; ++li) 
        {
            if(length(lightColors[li]) >= 0.0001){
                // calculate per-light radiance
                vec3 L = normalize(lightPositions[li].xyz - VertexViewSpace.xyz);
                vec3 H = normalize(V + L);

                float dist    = length(lightPositions[li].xyz - VertexViewSpace.xyz);
                float attenuation = 1.0 / (dist * dist);
                vec3 radiance     = lightColors[li] * attenuation;        
                
                // cook-torrance brdf
                float NDF = DistributionGGX(N, H, roughness);        
                float G   = GeometrySmith(N, V, L, roughness);      
                vec3 F    = fresnelSchlick(max(dot(N, V), 0.0), F0)/noLobes;       
                
                vec3 kS = F;
                vec3 kD = max((vec3(1.0) - kS)*(1.0 - metallic),0);
                
                vec3 numerator    =   NDF*G*F;
                float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
                vec3 specular     = numerator / max(denominator, 0.0000001);  
                    
                // add to outgoing radiance Lo
                float NdotL =  max(dot(N, L), 0.0);                
                Lo = Lo+ alpha* (kD * albedo / PI + specular) * NdotL ;//* radiance ; 
            }
        }    
    
        vec3 ambient = vec3(0.01/noLobes) * albedo * ao;
        color = color + ambient + Lo;
    }
	
    //tonemapping
    color = color / (color + vec3(1.0));
    //exposure
    color = vec3(1.0) - exp(-color*exposure);
    //gamma correction
    color = pow(color, vec3(1.0/gamma));  
   
    FragColor = vec4(color, 1.0);
}  

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
	
    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float num   = NdotV;
    float denom = NdotV * (1.0 - roughness) + roughness;
	
    return num / denom;
}
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, k);
    float ggx1  = GeometrySchlickGGX(NdotL, k);
	
    return ggx1 * ggx2;
}
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}  