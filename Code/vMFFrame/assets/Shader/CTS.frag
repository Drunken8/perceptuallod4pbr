#version 330 core
out vec4 FragColor;
in vec2 TexCoords;
in vec3 VertexViewSpace;
in vec3 NormalViewSpace;
in vec3 TangentViewSpace;
in vec3 BitangentViewSpace;
uniform sampler2D TexNormal;

// material parameters
uniform sampler2D TexAlbedo;
uniform sampler2D TexMetallic;
uniform sampler2D TexRoughness;
uniform sampler2D TexAo;

// lights
uniform vec3 lightColors[4];
uniform vec4 lightPositions[4];

const float PI = 3.14159265359;
const float gamma = 2.2;
const float exposure = 1.0;
  
float DistributionGGX(vec3 N, vec3 H, float roughness);
float GeometrySchlickGGX(float NdotV, float roughness);
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness);
vec3 fresnelSchlick(float cosTheta, vec3 F0);

void main()
{		

    vec3 norm = normalize(texture2D(TexNormal, TexCoords).rgb*2.0-1.0);
    vec3 albedo = texture2D(TexAlbedo, TexCoords).rgb;//, vec3(gamma));
    float metallic = texture2D(TexMetallic, TexCoords).r;
    float roughness = texture2D(TexRoughness, TexCoords).r;
    float ao = texture2D(TexAo, TexCoords).r;

    vec3 N = normalize((TangentViewSpace * norm.x) + (BitangentViewSpace * norm.y) + (NormalViewSpace * norm.z));
    vec3 V = normalize(-VertexViewSpace.xyz);

    vec3 F0 = vec3(0.04); 
	F0 = mix(F0, albedo, metallic);

    vec3 Lo = vec3(0.0);
    for(int i = 0; i < 1; ++i) 
    {
        if(length(lightColors[i]) >= 0.0001){
            // calculate per-light radiance
            vec3 L = normalize(lightPositions[i].xyz - VertexViewSpace.xyz);
            vec3 H = normalize(V + L);

            float dist    = length(lightPositions[i].xyz - VertexViewSpace.xyz);
            float attenuation = 1.0 / (dist * dist);
            vec3 radiance     = lightColors[i] * attenuation;        
            
            // cook-torrance brdf
            float NDF = DistributionGGX(N, H, roughness);        
            float G   = GeometrySmith(N, V, L, roughness);      
            vec3 F    = fresnelSchlick(max(dot(N, V), 0.0), F0);       
            
            vec3 kS = F;
            vec3 kD = max((vec3(1.0) - kS)*(1.0 - metallic),0);
            
            vec3 numerator    =   NDF*G*F;
            float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
            vec3 specular     = numerator / max(denominator, 0.0000001) ;  
                
            // add to outgoing radiance Lo
            float NdotL =  max(dot(N, L), 0.0);                
            Lo = Lo+ (kD * albedo / PI + specular) * NdotL;// * radiance ; 
        }
    }   

    vec3 ambient = vec3(0.01) * albedo*ao;

    vec3 color = ambient+Lo;
	
    //tonemapping
    color = color / (color + vec3(1.0));
    //exposure
    color = vec3(1.0) - exp(-color*exposure);
    //gamma correction
    color = pow(color, vec3(1.0/gamma));  
   
    FragColor = vec4(color, 1.0);
}  

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
	
    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    //float k = roughness*roughness/2;

    float num   = NdotV;
    float denom = NdotV * (1.0 - roughness) + roughness;
	
    return num / denom;
}
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;
    
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, k);
    float ggx1  = GeometrySchlickGGX(NdotL, k);
	
    return ggx1 * ggx2;
}
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}  