#version 400
out vec2 TexCoords;
out vec3 VertexViewSpace;
out vec3 NormalViewSpace;
out vec3 TangentViewSpace;

//passed by cinder
uniform mat4	ciModelView;
uniform mat4    ciViewMatrix;
uniform mat3	ciNormalMatrix;
uniform mat4	ciModelViewProjection;
in vec4			ciPosition;
in vec3			ciNormal;
in vec3			ciTangent;
in vec2			ciTexCoord0;


// Vertex shader main function
void main( void )
{	
	//transform to ViewSpace
    VertexViewSpace = (ciModelView*ciPosition).xyz;
	NormalViewSpace = normalize(ciNormalMatrix * ciNormal);
	TangentViewSpace = normalize(ciNormalMatrix * ciTangent);
	
	//pass textureCoordinates through
	TexCoords = ciTexCoord0;
	
	gl_Position = ciModelViewProjection * ciPosition;   
}