#version 400
out vec2 TexCoords;
out vec3 VertexViewSpace;
out vec3 NormalViewSpace;
out vec3 TangentViewSpace;
out vec3 BitangentViewSpace;

//passed by cinder
uniform mat4	ciModelView;
uniform mat4    ciViewMatrix;
uniform mat3	ciNormalMatrix;
uniform mat4	ciModelViewProjection;
in vec4			ciPosition;
in vec3			ciNormal;
in vec3			ciTangent;
in vec2			ciTexCoord0;


// Vertex shader main function
void main( void )
{	
    VertexViewSpace = (ciModelView*ciPosition).xyz;
	TexCoords = ciTexCoord0 *7;

	NormalViewSpace = normalize(ciNormalMatrix * ciNormal);
	TangentViewSpace = normalize(ciNormalMatrix * ciTangent);
	BitangentViewSpace = normalize(cross(NormalViewSpace,TangentViewSpace));

	gl_Position = ciModelViewProjection * ciPosition;   
}