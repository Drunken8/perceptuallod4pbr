#version 150

uniform sampler2D	uDiffuseMap, uNormalMap;
// lights
uniform vec3 lightColors[4];
uniform vec4 lightPositions[4];


// inputs passed from the vertex shader
in vec4		VertexViewSpace;
in vec3		NormalViewSpace, TangentViewSpace, BitangentViewSpace;
in vec2		TexCoord0;

// output a single color
out vec4			oColor;

const float PI = 3.14159265359;
const float gamma = 2.2;
const float exposure = 1.0;
const float SpecularExponent = 20;
const float SpecularCoefficient = 0.99;
const float DiffuseCoefficient = 0.01;


void main()
{
	vec3 p = vec3(0);
    for(int i = 0; i < 1; ++i) 
    {
		// fetch the normal from the normal map
		vec3 vMappedNormal = normalize(texture(uNormalMap, TexCoord0.st).rgb * 2.0 - 1.0);

		// modify it using the normal & tangents from the 3D mesh (normal mapping)
		vec3 normal = normalize((TangentViewSpace * vMappedNormal.x) + (BitangentViewSpace * vMappedNormal.y) + (NormalViewSpace * vMappedNormal.z));

		vec3 vToCamera = normalize( -VertexViewSpace.xyz );
		vec3 light = normalize( lightPositions[i].xyz - VertexViewSpace.xyz );
		//vec3 reflect = normalize(-reflect(light, normal));
        
        float distance    = length(lightPositions[i].xyz - VertexViewSpace.xyz);
        float attenuation = 1.0 / (distance * distance);
        vec3 radiance     = lightColors[i] * attenuation;  
        
        vec3 vhalf = normalize(vToCamera + light);
        
        
        float fDiffuse = max( dot(normal, light), 0.0 );
        fDiffuse = clamp( fDiffuse, 0.01, 1.0 );
        float norm = (SpecularExponent+1)/(2*PI);
        float BSpecular =  norm * pow(max(dot(vhalf, normal),0), SpecularExponent);
        
        p = p + texture2D(uDiffuseMap, TexCoord0).xyz * (SpecularCoefficient * BSpecular + DiffuseCoefficient) * fDiffuse;// * attenuation;
	}
		
		
	// output colors to buffer
	vec3 color = (p).rgb ;
	
    //tonemapping
    color = color / (color + vec3(1.0));
    //exposure
    color = vec3(1.0) - exp(-color*exposure);
    //gamma correction
    color = pow(color, vec3(1.0/gamma));  
   
    oColor.rgb = color;
	oColor.a = 1.0;
}