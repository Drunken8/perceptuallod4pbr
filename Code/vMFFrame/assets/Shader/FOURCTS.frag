#version 330 core
out vec4 FragColor;
in vec2 TexCoords;
in vec3 VertexViewSpace;
in vec3 NormalViewSpace;
in vec3 TangentViewSpace;
in vec3 BitangentViewSpace;
uniform sampler2D TexNormal;

// material parameters
uniform sampler2D TexAlbedo;
uniform sampler2D TexMetallic;
uniform sampler2D TexRoughness;
uniform sampler2D TexAo;

// lights
uniform vec3 lightColors[4];
uniform vec4 lightPositions[4];

const float PI = 3.14159265359;
const float gamma = 2.2;
const float exposure = 1.0;
  
vec3 fresnelSchlick(float cosTheta, vec3 F0);
float specularTerm(float roughness, float nDotH);

void main()
{		

    vec3 norm = normalize(texture2D(TexNormal, TexCoords).rgb*2.0-1.0);
    vec3 albedo = texture2D(TexAlbedo, TexCoords).rgb;//, vec3(gamma));
    float metallic = texture2D(TexMetallic, TexCoords).r;
    float roughness = texture2D(TexRoughness, TexCoords).r;
    float ao = texture2D(TexAo, TexCoords).r;

    vec3 TangentViewSpaceN = normalize(TangentViewSpace);
    vec3 NormalViewSpaceN = normalize(NormalViewSpace);
    vec3 BitangentViewSpaceN = normalize(BitangentViewSpace);

    vec3 N = normalize((TangentViewSpaceN * norm.x) + (BitangentViewSpaceN * norm.y) + (NormalViewSpaceN * norm.z));
    vec3 V = normalize(-VertexViewSpace.xyz);

    vec3 F0 = vec3(0.04); 
	F0 = mix(F0, albedo, metallic);

    vec3 Lo = vec3(0.0);
    for(int i = 0; i < 1; ++i) 
    {
        if(length(lightColors[i]) >= 0.0001){
            // calculate per-light radiance
            vec3 L = normalize(lightPositions[i].xyz - VertexViewSpace.xyz);
            vec3 H = normalize(V + L);
            
            // cook-torrance brdf
            float S = specularTerm(roughness,1-max(dot(N,H),0));
            vec3 F    = fresnelSchlick(max(dot(N, V), 0.0), F0);       
            
            vec3 kS = F;
            vec3 kD = max((vec3(1.0) - kS)*(1.0 - metallic),0);
            vec3 numerator    = F*S;
            float denominator = 4.0 * (1-max(dot(N, V), 0)) * (1-max(dot(N, L), 0));
            vec3 specular     = numerator / max(denominator, 0.0000001) ;  
                
            // add to outgoing radiance Lo
            float NdotL =  max(dot(N, L), 0.0);                
            Lo = Lo+ albedo* (kD +  kS*specular) * NdotL; 
        }
    }   

    vec3 ambient = vec3(0.01) * albedo*ao;
    vec3 color = Lo+ambient;

    
    //tonemapping
    color = color / (color + vec3(1.0));
    //exposure
    color = vec3(1.0) - exp(-color*exposure);
    //gamma correction
    color = pow(color, vec3(1.0/gamma));  
    FragColor = vec4(color, 1.0);
}  

float specularTerm(float roughness, float nDotH){
    float n = 1.0/(PI*pow(roughness,2.0));
    float e = exp(-pow(nDotH/roughness,2.0));
    return n*e;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}  