#version 400
layout (location = 0) out vec4 gEyeDirViewSpace;
layout (location = 1) out vec4 gNormalViewSpace;
layout (location = 2) out vec4 gTexCoords;
layout (location = 3) out vec4 gTangentViewSpace;
layout (location = 4) out vec4 gLightDirViewSpace;

//from vertex shader
in vec2 TexCoords;
in vec3 VertexViewSpace;
in vec3 NormalViewSpace;
in vec3 TangentViewSpace;

// lights
uniform vec3 lightColors[4];
uniform vec4 lightPositions[4];

//only used to estimate LoD, #LoDs has to match later used ones
uniform sampler2D TexAlbedo0; 
//specular is assumed to be constant 
//(especially because the results are later used for Physically based rendering)


void main()
{    
    //pass Normal and Position
	gNormalViewSpace.rgb = (NormalViewSpace + 1.0) /2.0;
    gNormalViewSpace.a = 1;
	gEyeDirViewSpace.rgb = (normalize(-VertexViewSpace) + 1.0) /2.0;
	gEyeDirViewSpace.a = 1;
	gTangentViewSpace.rgb = (TangentViewSpace + 1.0) /2.0;
	gTangentViewSpace.a = 1;

	
    gLightDirViewSpace.rgb = (normalize( lightPositions[0].xyz - VertexViewSpace ) + 1.0) /2.0;
    gLightDirViewSpace.a = 1;
    
	//pass TexCoords and extend with level
	gTexCoords.rg = (TexCoords*7.0)%1.0;
	gTexCoords.b = clamp(textureQueryLod(TexAlbedo0, TexCoords*7.0).x / 10.0,0,1);
	gTexCoords.a = 1;
	
}  