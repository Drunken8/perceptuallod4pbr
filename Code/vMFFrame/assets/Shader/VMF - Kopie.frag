#version 400

// Variables passed in by the vertex shader
in vec4 VertexViewSpace;
in vec3 NormalViewSpace;
in vec3 TangentViewSpace;
in vec3 BitangentViewSpace;
in vec2 TexCoord0;

// Varialbes passed in by the programe
//vMFs for Normals
uniform sampler2D vMFmap1;
uniform sampler2D vMFmap2;
uniform sampler2D vMFmap3;
uniform sampler2D vMFmap4;
//uniform sampler2D vMFmap5;
//uniform sampler2D vMFmap6;

//divusemaps for colour
uniform sampler2D uDiffuseMap1;
uniform sampler2D uDiffuseMap2;
uniform sampler2D uDiffuseMap3;
uniform sampler2D uDiffuseMap4;
//uniform sampler2D uDiffuseMap5;
//uniform sampler2D uDiffuseMap6;

const float PI = 3.14159265359;
const float gamma = 2.2;
const float exposure = 1.0;
const float SpecularExponent = 20*4;
const float SpecularCoefficient = 0.99;
const float DiffuseCoefficient = 0.01;

// lights
uniform vec3 lightColors[4];
uniform vec4 lightPositions[4];

//output a color per fragment
out vec4 oColor;

// Fragment main function
void main( void )
{  

	vec3  EyeDir = normalize(-VertexViewSpace.xyz);

	// Look up vMF lobes (6 lobes here) in 2D textures
	vec4 coeffs[6];
	vec3 colors[6];

	coeffs[0] = texture2D(vMFmap1, TexCoord0);
	coeffs[1] = texture2D(vMFmap2, TexCoord0);
	coeffs[2] = texture2D(vMFmap3, TexCoord0);
	coeffs[3] = texture2D(vMFmap4, TexCoord0);
	//coeffs[4] = texture2D(vMFmap5, TexCoord0);
	//coeffs[5] = texture2D(vMFmap6, TexCoord0);
	
	colors[0] = texture2D(uDiffuseMap1, TexCoord0).xyz;
	colors[1] = texture2D(uDiffuseMap2, TexCoord0).xyz;
	colors[2] = texture2D(uDiffuseMap3, TexCoord0).xyz;
	colors[3] = texture2D(uDiffuseMap4, TexCoord0).xyz;
	//colors[4] = texture2D(uDiffuseMap5, TexCoord0).xyz;
	//colors[5] = texture2D(uDiffuseMap6, TexCoord0).xyz;

	vec3 p = vec3(0);
	
    for (int i=0; i<4; i++) {
        //prepare lobe
        vec3 mu = coeffs[i].xyz*2.0-1.0;//map lobe from color space to real space
		float alpha = coeffs[i].w;
        mu /= alpha;
        float r = length(mu);
        float kappa = (3.0*r - r*r*r)/(1.0 - r*r);
        mu = normalize(mu); 
		float sprime = (kappa * SpecularExponent) / (kappa + SpecularExponent);

        //prepare material parameters
        vec3 Normal = mu;
        vec3 albedo = colors[i];//pow(colors[i], vec3(gamma))*alpha; //map color to linear space and apply scaling with alpha

        vec3 N = normalize((TangentViewSpace * Normal.x) + (BitangentViewSpace * Normal.y) + (NormalViewSpace * Normal.z));
       
        // reflectance equation
        for(int li = 0; li < 1; ++li) 
        {
			// modify it using the normal & tangents from the 3D mesh (normal mapping)
			vec3 normal = N;
			vec3 vToCamera = EyeDir;
			vec3 light = normalize( lightPositions[li].xyz - VertexViewSpace.xyz );
			//vec3 reflect = normalize(-reflect(light, normal));
			
			float distance    = length(lightPositions[li].xyz - VertexViewSpace.xyz);
			float attenuation = 1.0 / (distance * distance);
			vec3 radiance     = lightColors[li] * attenuation;  
			
			vec3 vhalf = normalize(vToCamera + light);
			
			
			float fDiffuse = max( dot(normal, light), 0.0 );
			fDiffuse = clamp( fDiffuse, 0.01, 1.0 );
			float norm = (sprime+1)/(2*PI);
			float BSpecular =  norm * pow(max(dot(vhalf, normal),0), sprime);
			
			p = p + albedo * alpha* (SpecularCoefficient * BSpecular + DiffuseCoefficient) * fDiffuse;// * attenuation;
		}
	}
		
		
	// output colors to buffer
	vec3 color = (p).rgb ;
	
    //tonemapping
    color = color / (color + vec3(1.0));
    //exposure
    color = vec3(1.0) - exp(-color*exposure);
    //gamma correction
    color = pow(color, vec3(1.0/gamma));  
   
    oColor.rgb = color;
	oColor.a = 1.0;
}
