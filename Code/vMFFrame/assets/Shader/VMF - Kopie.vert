#version 400

//passed by cinder
uniform mat4	ciModelView;
uniform mat3	ciNormalMatrix;
uniform mat4	ciModelViewProjection;
in vec4			ciPosition;
in vec3			ciNormal;
in vec3			ciTangent;
in vec2			ciTexCoord0;

// Variables passed to fragment shader
out vec3 LightPos;
out vec4 VertexViewSpace;
out vec3 NormalViewSpace;
out vec3 TangentViewSpace;
out vec3 BitangentViewSpace;
out vec2 TexCoord0;

// Varialbes passed in by the programe

// Vertex shader main function
void main( void )
{	
	// Standard transformation
	VertexViewSpace = ciModelView * ciPosition;
    
	// Vertex's local coordinate axis
	NormalViewSpace = normalize(ciNormalMatrix * ciNormal);
	TangentViewSpace = normalize(ciNormalMatrix * ciTangent);
	BitangentViewSpace = normalize(cross(NormalViewSpace,TangentViewSpace));

	TexCoord0 = ciTexCoord0;

	gl_Position = ciModelViewProjection * ciPosition;
}

