#version 400
out vec4 FragColor;
in vec2 TexCoords;
in vec3 VertexViewSpace;
in vec3 NormalViewSpace;
in vec3 TangentViewSpace;
in vec3 BitangentViewSpace;

uniform sampler2D TexNormal0;
uniform sampler2D TexNormal1;
uniform sampler2D TexNormal2;
uniform sampler2D TexNormal3;
uniform sampler2D TexNormal4;
uniform sampler2D TexNormal5;

// material parameters
uniform sampler2D TexAlbedo0;
uniform sampler2D TexAlbedo1;
uniform sampler2D TexAlbedo2;
uniform sampler2D TexAlbedo3;
uniform sampler2D TexAlbedo4;
uniform sampler2D TexAlbedo5;
uniform sampler2D TexMats0; //r is metal, g is roughness, b is AO
uniform sampler2D TexMats1;
uniform sampler2D TexMats2;
uniform sampler2D TexMats3;
uniform sampler2D TexMats4;
uniform sampler2D TexMats5;

// lights
uniform vec3 lightColors[4];
uniform vec4 lightPositions[4];

const float PI = 3.14159265359;
const float gamma = 2.2;
const float exposure = 1.0;
const int noLobes = 4;
  
vec3 fresnelSchlick(float cosTheta, vec3 F0);
float specularTerm(float roughness, float nDotH);

void main()
{		

    vec4 coeffs[6];
    vec3 colors[6];
    vec3 mat[6];

	coeffs[0] = texture2D(TexNormal0, TexCoords);
	coeffs[1] = texture2D(TexNormal1, TexCoords);
	coeffs[2] = texture2D(TexNormal2, TexCoords);
	coeffs[3] = texture2D(TexNormal3, TexCoords);
	coeffs[4] = texture2D(TexNormal4, TexCoords);
	coeffs[5] = texture2D(TexNormal5, TexCoords);

	colors[0] = texture2D(TexAlbedo0, TexCoords).xyz;//, vec3(gamma));
	colors[1] = texture2D(TexAlbedo1, TexCoords).xyz;//, vec3(gamma));
	colors[2] = texture2D(TexAlbedo2, TexCoords).xyz;//, vec3(gamma));
	colors[3] = texture2D(TexAlbedo3, TexCoords).xyz;//, vec3(gamma));
	colors[4] = texture2D(TexAlbedo4, TexCoords).xyz;//, vec3(gamma));
	colors[5] = texture2D(TexAlbedo5, TexCoords).xyz;//, vec3(gamma));

	mat[0] = texture2D(TexMats0, TexCoords).xyz;
	mat[1] = texture2D(TexMats1, TexCoords).xyz;
	mat[2] = texture2D(TexMats2, TexCoords).xyz;
	mat[3] = texture2D(TexMats3, TexCoords).xyz;
	mat[4] = texture2D(TexMats4, TexCoords).xyz;
	mat[5] = texture2D(TexMats5, TexCoords).xyz;

    vec3 color = vec3(0);
    vec3 V = normalize(-VertexViewSpace.xyz);

    vec3 TangentViewSpaceN = normalize(TangentViewSpace);
    vec3 NormalViewSpaceN = normalize(NormalViewSpace);
    vec3 BitangentViewSpaceN = normalize(BitangentViewSpace);
    float asum = 0;

    for (int i=0; i<noLobes; i++) {
        //prepare lobe
        vec3 mu = coeffs[i].xyz*2.0-1.0;//map lobe from color space to real space
        float alpha = coeffs[i].w;
        mu = mu /  alpha;
        float r = length(mu);
        mu = normalize(mu); 
        float kappa = (3.0*r - r*r*r)/(1.0 - r*r);

        //prepare material parameters
        vec3 Normal = mu;
        vec3 albedo = colors[i];//, vec3(gamma))*alpha; //map color to linear space and apply scaling with alpha
        float metallic = mat[i].r;
        float roughness =sqrt(mat[i].g*mat[i].g+1/(2*kappa)); //scale roughness to fit lobes
        float ao = mat[i].b;

        vec3 N = normalize((TangentViewSpaceN * Normal.x) + (BitangentViewSpaceN * Normal.y) + (NormalViewSpaceN * Normal.z));
       
        vec3 F0 = vec3(0.04)*alpha; 
           
        F0 = mix(F0, albedo, metallic);
        albedo = alpha*albedo;
        // reflectance equation
        vec3 Lo = vec3(0.0);
        for(int li = 0; li < 1; ++li) 
        {
            if(length(lightColors[li]) >= 0.0001){ 
                // calculate per-light radiance
                vec3 L = normalize(lightPositions[li].xyz - VertexViewSpace.xyz);
                vec3 H = normalize(V + L);
                
                // cook-torrance brdf
                float S = specularTerm(roughness,1-max(dot(N,H),0));
                vec3 F    = fresnelSchlick(max(dot(N, V), 0.0), F0);       
                
                vec3 kS = F;
                vec3 kD = max((vec3(1.0) - kS)*(1.0 - metallic),0);
                vec3 numerator    = F*S;
                float denominator = max(4.0 * (1-max(dot(N, V), 0)) * (1-max(dot(N, L), 0)),0.0000001) / alpha;
                vec3 specular     = numerator / denominator;
                    
                // add to outgoing radiance Lo
                float NdotL =  max(dot(N, L), 0.0);   
                
                Lo = Lo+ albedo* (kD +  kS*specular) * NdotL; 
            }
        }    
        //FragColor += vec4(metallic,roughness,ao,1);
        vec3 ambient = vec3(0.01) * albedo * ao;
        color = color + ambient + Lo;
    }
    
    //tonemapping
    color = color / (color + vec3(1.0));
    //exposure
    color = vec3(1.0) - exp(-color*exposure);
    //gamma correction
    color = pow(color, vec3(1.0/gamma));  
    FragColor = vec4(color, 1.0);
}  

float specularTerm(float roughness, float nDotH){
    float n = 1.0/(PI*pow(roughness,2.0));
    float e = exp(-pow(nDotH/roughness,2.0));
    return n*e;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}  