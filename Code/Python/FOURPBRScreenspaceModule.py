import torch
import torch.nn as nn
import numpy as np
import torch.nn.functional as F
import math
import numpngw
from skimage import io

import torchvision

class ScreenSpaceShading(nn.Module):
    """
        performs screenspace shading for Frequency Domain Normal Map Filtering,
        as described in "Frequency Domain Normal Map Filtering" by Han et. al
        The goal is a set of optimised Lobe-Textures to represent the Normals
        optimal in an perceptual sense.
        As loss is a Method from "The Unreasonable Effectiveness of Deep Features as a Perceptual Metric" 
        suggested. Additional losses like maximizing difference between lobes and minimizing difference between lods
        may be needed.
    
        The only optimizable Variables should be the lobe textures. For other Methodes (PBR) it may be useful to
        optimize additional weights for material properties in case they are not easily computed analytically.
        
        _lobes will contain #noLobes optimized Textures in TangentSpace (like standart NormalMaps) with the additional 4th component
    
        Input are the textures from deferred rendering:
            -3D Texture-Coordinates (0)
            -3D EyeDirViewSpace-Directions (1)
            -3D NormalViewspace-Directions (2)
            -3D Tangentviewspace-Drictions (3)
            -3D Color-Vector (albedo) (4)
    """

    def makeLobeTexture(self, dim, device):
        return nn.Parameter(torch.cat((torch.ones((1,1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)/2, #tangent dir
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)/2, #bitangent dir
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)*0.625, #normal dir
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)/4, #alpha
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)/2, #color r
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)/2, #color g
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)/2, #color b
            torch.zeros((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False), #metallic init as none metal
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)*0.4, #roughness init as flat
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False) #ao init as no AO-> no shadow
            ), dim=1).requires_grad_())
    
    def getParamsAsFlatList(self):
        return [item for sublist in self._lobes for item in sublist]

    def getLobesOfLOD(self, LOD, detached = True):
        tmp = []
        for i in range(self._noLobes):
            if(detached):
                tmp.append(self._lobes[i][LOD].detach())
            else:
                tmp.append(self._lobes[i][LOD])
        return torch.cat(tmp, dim= 0)

    def __init__(self, device,albedo, dim = 512, noLobes = 4, usedBatchsize = 10, loadFromFile = False, initFromFile = False,setfromfile = False, runname = "default", runID = 0):
        super().__init__()
        self._device = device
        self._noLobes = noLobes
        self._exposure = 1.0
        self._gamma = 2.2
        self._dim = dim
        self._pi = 3.14159265359

        self._batchsize = usedBatchsize
        #setup trainable tensor with requires_grad=True in init for autograd 
        #shape is for each lobe dim*dim*3 Channels as a (normalized) direction and an additional alpha value
        #type is torch.float32 for standart 0-1 range, later represented with less bits

        self._lobes = []
        if(loadFromFile == False): #load model from file with priority
            for i in range(self._noLobes):
                dim = self._dim
                currLob = []
                clod = 0
                while(dim >= 1):
                    if(initFromFile == False): #reinit from mips
                        if(setfromfile == False):
                            #init blind as last resort
                            currLob.append(self.makeLobeTexture(dim, self._device))
                        else:
                            #load external solved lobes
                            currLob.append(self.setLobesFromFiles( self._device, i, clod,dim, resolution=255.0))
                    else:
                        currLob.append(self.initLobeTensorFromFile(dim,self._device))
                    
                    print("INIT LOB " + str(i) + " size " + str(dim) + " LOD " + str(clod))

                    dim = dim // 2  #do integer division
                    clod += 1
                self._lobes.append(currLob)
        else:
            self.loadLobeTensorsFromFile(math.log2(dim)+1,runID=runID, runname = runname)

    

    def warpMIP(self, MipMap, sampleImage):
        """ 
            Samples a MipMap (List of Texture levels (decreasing detail)) according to 3D Coordinates in sampleImage (analougus to grid_sample)
            return sampled and warped image

            Mipmap has shapeof List(noLods) Texture(BxCxWXH)
            sampleImage has shapeof Texture(BxWxHxC) where C is 3 in Range [-1,1] (3. Coord is corrected to [0,1])
        """
        #first warp every level indipendent
        warpedLevels = []
        for i in range(len(MipMap)): 
            currImg = torch.cat(sampleImage[:,:,:,0:2].shape[0]* [MipMap[i]])
            warpedImg = F.grid_sample(currImg, sampleImage[:,:,:,0:2], mode='bilinear', padding_mode='reflection', align_corners=False)
            warpedLevels.append(warpedImg)
            del warpedImg
            del currImg
        warpedTensor = torch.stack(warpedLevels,dim = 0)
        for i in reversed(range(len(warpedLevels))):
            del warpedLevels[i]
        del warpedLevels
        torch.cuda.empty_cache()
        
        #make a mask to weight entries,
        correctedSampleImage = (sampleImage[:,:,:,2:].permute(0,3,1,2)  + 1) /2 #is in -1 to 1, needs to be in 0 to 1...
        w = torch.cat(warpedTensor.shape[0]*[correctedSampleImage*(len(MipMap)-1)], dim = 1)
        del sampleImage
        del correctedSampleImage
        torch.cuda.empty_cache()
        weights = []
        for i in range(len(MipMap)): 
            tmp = (w - i) #get difference
            tmp.abs_()
            tmp = 1 - tmp
            mask = (tmp < 0) | (tmp > 1)
            tmp[mask] = 0 #erase to big or to small fractions for this lod
            del mask
            weights.append(tmp)
        weightTensor = torch.stack(weights, dim = 0)
        for i in reversed(range(len(weights))):
            del weights[i]
        del weights
        torch.cuda.empty_cache()

        #weight entries
        weightedwarpedTensor = warpedTensor*weightTensor #torch.where((weightTensor <1) &(weightTensor > 0 ),warpedTensor*weightTensor,warpedTensor*0)
        del warpedTensor
        del weightTensor

        #reduce with sum to finish interpolation
        weightedwarpedreducedTensor = weightedwarpedTensor.sum(dim=0)
        del weightedwarpedTensor
        torch.cuda.empty_cache()
        return weightedwarpedreducedTensor


    def saveLobeTensorsToFile(self, runID = None):
        with torch.no_grad():
            for i in range(len(self._lobes)):
                #save each Lobe
                for j in range(len(self._lobes[i])):
                    if runID is None:
                        torch.save(self._lobes[i][j], "./Code/Python/backup/" + str(i) + " " + str(j) + ".pt")
                    else:
                        torch.save(self._lobes[i][j], "./Code/Python/intermediate/"+str(runID) + " " + str(i) + " " + str(j) + ".pt")
                
        
    def loadLobeTensorsFromFile(self, noLovels = 10, runID = None,runname = "intermediate"):
        self._lobes.clear()
        print("cleared _lobes, loading from file")
        for i in range(self._noLobes):
            currList = []
            for j in range(int(noLovels)):
                
                if runID is None:
                    curr = torch.load( "./Code/Python/backup/" + str(i) + " " + str(j) + ".pt")
                else:
                    curr = torch.load( "./Code/Python/"+ runname +"/"+str(runID) + " " + str(i) + " " + str(j) + ".pt")
                curr.requires_grad_()
                currList.append(curr)
            self._lobes.append(currList)

    def initLobeTensorFromFile(self,dimension,device):
        normals = io.imread("./Code/vMFFrame/assets/tst_normal_combined.jpg")[:,:,0:3]
        albedo = io.imread("./Code/vMFFrame/assets/tst_albedo.png")[:,:,0:3]
        metallic = io.imread("./Code/vMFFrame/assets/tst_metallic.png")
        roughness = io.imread("./Code/vMFFrame/assets/tst_roughness.png")
        ao = io.imread("./Code/vMFFrame/assets/tst_ao.png")
        
        normals = ((torch.from_numpy(np.array(normals).astype(np.float32)).unsqueeze(0).permute(0,3,1,2).flip(2)/ 255.0  *2 -1 )*0.25 +1)/2
        albedo = torch.from_numpy(np.array(albedo).astype(np.float32)).unsqueeze(0).permute(0,3,1,2).flip(2)/ 255.0
        metallic = torch.from_numpy(np.array(metallic).astype(np.float32)).unsqueeze(-1).unsqueeze(0).permute(0,3,1,2).flip(2)/ 255.0
        roughness = torch.from_numpy(np.array(roughness).astype(np.float32)).unsqueeze(-1).unsqueeze(0).permute(0,3,1,2).flip(2)/ 255.0
        ao = torch.from_numpy(np.array(ao).astype(np.float32)).unsqueeze(-1).unsqueeze(0).permute(0,3,1,2).flip(2)/ 255.0
        
        dim = normals.shape[2]
        alpha = torch.ones((1, 1,dim,dim), dtype=torch.float32, requires_grad=False)/4
        
        
        lob = torch.cat([normals, alpha, albedo, metallic, roughness, ao],dim = 1)
        del normals,albedo,metallic,roughness,ao
        return nn.Parameter(F.interpolate(lob,size = dimension).to(self._device).requires_grad_())

    def setLobesFromFiles(self, device, lobe, lod, targetdim, resolution = 65535):
        normals = io.imread("./OUTPUT/LOBE"+ str(lobe)+ "LOD" + str(lod) +"lobe.png")[:,:,0:4]
        albedo = io.imread("./OUTPUT/"+ str(lobe)+ "LOD" + str(lod) +"albedo.png")[:,:,0:3]
        material = io.imread("./OUTPUT/"+ str(lobe)+ "LOD" + str(lod) +"material.png")[:,:,0:3]
        
        normals = torch.from_numpy(np.array(normals).astype(np.float32)).unsqueeze(0).permute(0,3,1,2).flip(2)/ resolution
        albedo = torch.from_numpy(np.array(albedo).astype(np.float32)).unsqueeze(0).permute(0,3,1,2).flip(2)/ resolution
        material = torch.from_numpy(np.array(material).astype(np.float32)).unsqueeze(0).permute(0,3,1,2).flip(2)/ resolution
        
        lob = torch.cat([normals, albedo, material],dim = 1)
        del normals,albedo,material
        return nn.Parameter(F.interpolate(lob,size = targetdim).to(self._device).requires_grad_())

    def normSumAlpha(self):
        with torch.no_grad():
            for i in range(len(self._lobes[0])): #norm each Level
                curr = self.getLobesOfLOD(i,False)[:,3:4] #collect current level
                norms = torch.sum(curr,dim=0,keepdim=True) #calculate SUM
                for j in range(self._noLobes):
                    self._lobes[j][i][:,3:4] /= norms #normalize SUM -> length will be 1
                del norms
                del curr
        

    def clampAllLobesToColor(self):
        with torch.no_grad():
            for lob in self._lobes:
                for lod in lob:
                    lod.clamp_(0,1)



    #@profile
    def forward(self, input):
        """ 
            Input are the textures from deferred rendering
            Output is the shaded image
        """
        

        self.clampAllLobesToColor()
        self.normSumAlpha()


        texCoords = input[:,0].permute(0,2,3,1) * 2 - 1
        V = input[:,1]
        V = V/torch.norm(V,dim=1,keepdim=True)
        normalViewSpace = input[:,2]
        normalViewSpace = normalViewSpace/torch.norm(normalViewSpace,dim=1,keepdim=True)
        tangentViewSpace = input[:,3]
        tangentViewSpace = tangentViewSpace/torch.norm(tangentViewSpace,dim=1,keepdim=True)
        L = input[:,4]
        L = L/torch.norm(L,dim=1,keepdim=True)
        del input
        
        #compute bitangent for transformation
        biTangentViewSpace = torch.cross(normalViewSpace, tangentViewSpace, dim=1)
        lenBiTan = torch.clamp(torch.sqrt(torch.sum(torch.pow(biTangentViewSpace, 2), 1).unsqueeze(1)),0,1) #length of mu
        biTangentViewSpace = (biTangentViewSpace)/torch.cat([lenBiTan]*3,dim=1)
        
        #mask = torch.isnan(biTangentViewSpace)
        #biTangentViewSpace[mask] = 0 #erase nans
        #del mask
        #torch.cuda.empty_cache()

        color = torch.zeros((texCoords.shape[0],3,self._dim, self._dim), dtype=torch.float32, device=self._device, requires_grad=False)
        for i in range(self._noLobes):
            #prepare lobes
            torch.cuda.empty_cache()
            warpedLobes = self.warpMIP(self._lobes[i], texCoords)#F.grid_sample(torch.cat(self._batchsize* [self._lobeParam[i:i+1]]), texCoords[:,:,:,0:2], mode='bilinear', padding_mode='reflection', align_corners=False)
           
            alpha = torch.clamp( warpedLobes[:, 3:4] ,1e-10,1.0)
            r = torch.clamp(2.0 * warpedLobes[:, 0:3] - 1.0,-1.0,1.0)
            albedo = warpedLobes[:, 4:7]
            metallic = warpedLobes[:, 7:8]
            roughness = warpedLobes[:, 8:9]
            ao = warpedLobes[:, 9:]
            del warpedLobes
            torch.cuda.empty_cache()
            
            r = r / alpha
            
            F0 = ((1-metallic) * 0.04*alpha + (metallic)*(albedo)) #TODO maybe 0.04*alpha
            
            albedo = albedo*alpha

            lenr = torch.clamp(torch.norm(r,dim=1,keepdim=True),1e-6 ,0.9999 )#torch.clamp(torch.sqrt(torch.sum(torch.pow(r, 2), 1,keepdim=True)),min=0,max=1) #length of mu
            mu = r/torch.norm(r,dim=1,keepdim=True)

            kappa =  (3.0*lenr - lenr*lenr*lenr)/(1.0 - lenr*lenr)
            roughness = torch.sqrt(torch.pow(roughness,2)+1/(2*kappa))

            N = tangentViewSpace * mu[:,0:1] + biTangentViewSpace * mu[:,1:2] + normalViewSpace * mu[:,2:]   
            N = N/torch.norm(N,dim=1,keepdim=True)
            del mu,kappa,lenr,r

            #vec3 vhalf = normalize(vToCamera + light);
            H = V+L
            H = H/torch.norm(H,dim=1,keepdim=True)
            

            #NDF, G, F
            NDotH = torch.clamp(torch.sum(N*H,1,keepdim=True),0,1)
            NDotV = torch.clamp(torch.sum(N*V,1,keepdim=True),0,1)
            S = self.specularTerm(roughness,(1-NDotH))
            Fres = self.fresnelSchlick(NDotV, F0)
            #KS+KD
            kD = torch.clamp((1-Fres)*(1-metallic),min = 0)
            NDotL = torch.clamp(torch.sum(N*L,1,keepdim=True),0,1)
            nomi = S*Fres
            denom = torch.clamp(4* (1-NDotV) * (1-NDotL), min = 1e-7)/alpha
            del NDotV,H,roughness,F0,metallic,N
            specular = nomi  / denom
            
            #Accumulation
            color = color + albedo*(kD+Fres*specular) * NDotL + albedo * ao * 0.01
            
            del NDotL,denom,specular,kD,ao,albedo,Fres
            torch.cuda.empty_cache()


        #do tonemapping
        color = torch.where(color > 0,(color) / ((color) + 1),color*0+1e-6)
        color = torch.where(color > 0,1.0 - torch.exp(-(color)*self._exposure),color*0+1e-6)
        color = torch.where(color > 0, torch.pow(color.abs()+1e-10, 1/self._gamma),color*0+1e-6) 
        
        mask = (texCoords.permute(0,3,1,2)[:,1:2] == -1)
        color[torch.cat((mask, mask, mask), dim = 1)] = 0
        del mask,biTangentViewSpace,L,V,normalViewSpace,tangentViewSpace,texCoords
        torch.cuda.empty_cache()
        return color

    def getAlignment(self):
        totalDif = 0
        for lob in self._lobes: #calculate for each lobe
            for i in range(len(lob)-1): #difference between Lods
                currLOD = F.interpolate(lob[i][0:4], scale_factor=0.5) #scale current lod down to match lower lod
                totalDif += (currLOD-lob[i+1][0:4]).abs().mean() / len(lob) #difference is low if alignment is good, scale by numberof levels
                del currLOD
        return totalDif / self._noLobes # scale by number of lobes

    def specularTerm(self, roughness, nDotH):
        n = 1.0/ (self._pi*torch.pow(roughness,2.0))
        e = torch.exp(-torch.pow(nDotH/roughness,2.0))
        ne = n*e
        del n,e
        torch.cuda.empty_cache()
        return ne

    def outputLobesToFiles(self, path = "OUTPUT"):
        with torch.no_grad():
            self.normSumAlpha()
            self.clampAllLobesToColor()
            for l in range(10):
                thislod = self.getLobesOfLOD(l)
                for c in range(self._noLobes):
                    self.save_16bit_image(thislod[c,0:4].detach().flip(1),path+ "/LOBE" + str(c)  + "LOD" + str(l) + "lobe.png")
                    self.save_16bit_image(thislod[c,4:7].detach().flip(1),path+ "/LOBE" + str(c)  + "LOD" + str(l) + "albedo.png")
                    self.save_16bit_image(thislod[c,7:].detach().flip(1),path+ "/LOBE" + str(c)  + "LOD" + str(l) + "material.png")
                del thislod
                torch.cuda.empty_cache()
            

    def save_16bit_image(self, tensor, filename, nrow=8, padding=2,
               normalize=False, range=None, scale_each=False, pad_value=0):  
        grid = torchvision.utils.make_grid(tensor, nrow=nrow, padding=padding, pad_value=pad_value,
                normalize=normalize, range=range, scale_each=scale_each)
        ndarr = grid.to('cpu').mul_(65535).add_(0.5).clamp_(0, 65535).permute(1, 2, 0).to('cpu', torch.int32).numpy().astype(np.dtype('uint16'))
        numpngw.write_png(filename, ndarr)
        del grid, ndarr
        torch.cuda.empty_cache()
       
    def fresnelSchlick(self,cosTheta, F0):
        return F0 + (1.0-F0) * torch.pow(1.0-cosTheta, 5.0)