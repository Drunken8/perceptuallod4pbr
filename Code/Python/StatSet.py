import numpy as np
import glob
import os
from skimage import io
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, datasets
import torch

class StatDataset(Dataset):
    """"""

    #h5py performance
    
    def __init__(self, root_dir, device,names = [],  transform=None, bitDepthPerChannel = 8):
        """
            checks amount of data in root_dir and saves transform for later, no dataloading here
        """
        self._device = device
        self._names = names
        self._transform = transform
        #save samples only as list of filenames (later loading extends names)
        self._groundTruthPaths = glob.glob(root_dir +  "\*groundTruth.png") #load names from groundTruth as measure for length and ordering
        self._div = (2**bitDepthPerChannel)-1
        


    def __len__(self):
        return len(self._groundTruthPaths)

    def __getitem__(self, idx):
        #get item from list, extend path, replace groundTruth with other keys
        cname = self._groundTruthPaths[idx]

        #load images
        groundTruth = io.imread(cname,format='PNG-FI')
        groundTruth = torch.from_numpy(np.array(groundTruth).astype(np.float32)).unsqueeze(0)[:,:,:]/ self._div
        groundTruth = groundTruth[:,:,:,0:3].permute(0,3,1,2)

        sample = {"groundtruth": groundTruth}

        for name in self._names:
            tmp = io.imread(cname.replace("groundTruth", name),format='PNG-FI')
            tmp = torch.from_numpy(np.array(tmp).astype(np.float32)).unsqueeze(0)[:,:,:]/ self._div
            tmp = tmp[:,:,:,0:3].permute(0,3,1,2)
            sample[name] = tmp

        seed = cname.split('\\')[-1].replace("groundTruth.png", "")
        
        sample["seed"] = seed
        
        return sample