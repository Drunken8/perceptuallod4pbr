import numpy as np
import glob
import os
from skimage import io
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, datasets
import torch

class DeferredDataset(Dataset):
    """"""

    #h5py performance
    
    def __init__(self, root_dir, device,  transform=None, bitDepthPerChannel = 16):
        """
            checks amount of data in root_dir and saves transform for later, no dataloading here
        """
        self._device = device
        self._transform = transform
        #save samples only as list of filenames (later loading extends names)
        self._groundTruthPaths = glob.glob(root_dir +  "\*groundTruth.png") #load names from groundTruth as measure for length and ordering
        self._div = (2**bitDepthPerChannel)-1
        print("DIVISOR",self._div)


    def __len__(self):
        return len(self._groundTruthPaths)

    def getSeed(self,idx):
        cname = self._groundTruthPaths[idx]
        seed = cname.split('\\')[-1].replace("groundTruth.png", "")
        return seed


    def __getitem__(self, idx):
        #get item from list, extend path, replace groundTruth with other keys
        cname = self._groundTruthPaths[idx]
        
        #load images
        groundTruth = io.imread(cname,format='PNG-FI')
        texCoords = io.imread(cname.replace("groundTruth", "texCoords"),format='PNG-FI')
        eyeDirViewSpace = io.imread(cname.replace("groundTruth", "eyeDirViewSpace"),format='PNG-FI')
        normalViewSpace = io.imread(cname.replace("groundTruth", "normalViewSpace"),format='PNG-FI')
        tangentViewSpace = io.imread(cname.replace("groundTruth", "tangentViewSpace"),format='PNG-FI')
        lightDirViewSpace = io.imread(cname.replace("groundTruth", "lightDirViewSpace"),format='PNG-FI')
        #apply transform, as in https://discuss.pytorch.org/t/pil-image-to-floattensor-uint16-to-float32/54577, because no interpolation between Bytes
        groundTruth = torch.from_numpy(np.array(groundTruth).astype(np.float32)).unsqueeze(0)[:,:,:]/ 255.0
        texCoords = torch.from_numpy(np.array(texCoords).astype(np.float32)).unsqueeze(0)/ self._div
        eyeDirViewSpace = torch.from_numpy(np.array(eyeDirViewSpace).astype(np.float32)).unsqueeze(0)/ self._div *2.0 -1.0
        normalViewSpace = torch.from_numpy(np.array(normalViewSpace).astype(np.float32)).unsqueeze(0)/ self._div *2.0 -1.0
        tangentViewSpace = torch.from_numpy(np.array(tangentViewSpace).astype(np.float32)).unsqueeze(0)/ self._div *2.0 -1.0
        lightDirViewSpace = torch.from_numpy(np.array(lightDirViewSpace).astype(np.float32)).unsqueeze(0)/ self._div *2.0 -1.0
        
        groundTruth = groundTruth[:,:,:,0:3].permute(0,3,1,2)
        texCoords = texCoords.permute(0,3,1,2)
        eyeDirViewSpace = eyeDirViewSpace.permute(0,3,1,2)
        normalViewSpace = normalViewSpace.permute(0,3,1,2)
        tangentViewSpace = tangentViewSpace.permute(0,3,1,2)
        lightDirViewSpace = lightDirViewSpace.permute(0,3,1,2)
        
        seed = cname.split('\\')[-1].replace("groundTruth.png", "")
        
        #return in same order as needed for forward input
        sample = torch.cat((groundTruth, texCoords, eyeDirViewSpace,normalViewSpace,tangentViewSpace,lightDirViewSpace), dim = 0).to(self._device)
        return sample