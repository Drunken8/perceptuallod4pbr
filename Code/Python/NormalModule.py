import torch
import torch.nn as nn
import numpy as np
import torch.nn.functional as F
import math

import torchvision

class ScreenSpaceNormalShading(nn.Module):
    """
        makes screenspaceoptimazation for normal map
    """

    def makeLobeTexture(self, dim, device):
        return nn.Parameter(torch.cat((torch.ones((1,1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)*0.5,
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)*0.5,
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)), dim=1).requires_grad_())
    
    def getParamsAsFlatList(self):
        return [item for sublist in self._lobes for item in sublist]

    def getLobesOfLOD(self, LOD):
        tmp = []
        for i in range(self._noLobes):
            tmp.append(self._lobes[i][0].detach())
        return torch.cat(tmp, dim= 0)

    def __init__(self, device,albedo, dim = 512, noLobes = 1, usedBatchsize = 10, loadFromFile = False):
        super().__init__()
        self._device = device
        self._noLobes = noLobes
        self._spec = 20 #specular exponent
        self._specCoef = 0.99 #specular coefficient
        self._diffCoef = 0.01 #diffuse coefficient
        self._exposure = 1.0
        self._gamma = 2.2
        self._dim = dim
        self._albedo = torch.cat(usedBatchsize*[albedo.permute(0,3,1,2)])
        
        self._batchsize = usedBatchsize
        #setup trainable tensor with requires_grad=True in init for autograd 
        #shape is for each lobe dim*dim*3 Channels as a (normalized) direction and an additional alpha value
        #type is torch.float32 for standart 0-1 range, later represented with less bits

        self._lobes = []
        for i in range(self._noLobes):
            dim = self._dim
            currLob = []
            currLob.append(self.makeLobeTexture(dim, self._device))
            self._lobes.append(currLob)
    
        #print(len(self._lobes), len(self._lobes[0]), self._lobes[0][0].shape)
    

    def saveLobeTensorsToFile(self):
        pass
        
    def loadLobeTensorsFromFile(self, noLovels = 10):
        pass

    #@profile
    def forward(self, input):
        """ 
            Input are the textures from deferred rendering
            Output is the shaded image
        """
        
        texCoords = input[:,0].permute(0,2,3,1) * 2 - 1
        eyeDirViewSpace = input[:,1]
        normalViewSpace = input[:,2]
        tangentViewSpace = input[:,3]
        lightDirViewSpace = input[:,4]
        #torchvision.utils.save_image( texCoords.permute(0,3,1,2), "texCoords.png")
        albedo = F.grid_sample(self._albedo, texCoords[:,:,:,0:2], mode='bilinear', padding_mode='reflection', align_corners=False)
        
        #compute bitangent for transformation
        biTangentViewSpace = torch.cross(normalViewSpace, tangentViewSpace, dim=1)
        biTangentViewSpace = F.normalize(biTangentViewSpace, p=3, dim=1)
        biTangentViewSpace[torch.isnan(biTangentViewSpace)] = 0 #erase nans
        #torchvision.utils.save_image( (texCoords[:,:,:,2:].permute(0,3,1,2) +1 )/2, "level.png")
        if torch.isnan(normalViewSpace).any():
            print("normalViewSpace")

        color = torch.zeros((1,3,self._dim, self._dim), dtype=torch.float32, device=self._device, requires_grad=False)
        for i in range(self._noLobes):
            #prepare lobes
            warpedLobes = F.grid_sample(torch.cat(self._batchsize* [self._lobes[i][0]*2.0-1.0]), texCoords[:,:,:,0:2], mode='bilinear', padding_mode='reflection', align_corners=False)
            #torchvision.utils.save_image( warpedLobes[:,0:3,:,:], str(i) + "warpedLobes.png")
            
            mu =  F.normalize(warpedLobes, p=3, dim=1)

            #calculate modification for lobes
            sprime = self._spec
            norm = (sprime +1.0) / (2* math.pi)
            
            tanSpaceLobe = tangentViewSpace * mu[:,0:1] + biTangentViewSpace * mu[:,1:2] + normalViewSpace * mu[:,2:]
            
            tanSpaceLobe = F.normalize(tanSpaceLobe, p=3, dim=1)
            #tanSpaceLobe[torch.isnan(tanSpaceLobe)] = 0 #erase nans
           
            
            #reflect = lightDirViewSpace-2* torch.sum(lightDirViewSpace*tanSpaceLobe,1).unsqueeze(1) * tanSpaceLobe
            fDiffuse = torch.clamp(torch.sum(lightDirViewSpace*tanSpaceLobe,1).unsqueeze(1),0.01,1.0) 
            

            #vec3 vhalf = normalize(vToCamera + light);
            vHalf = F.normalize(eyeDirViewSpace + lightDirViewSpace)

            fSpecular = torch.clamp(torch.sum(vHalf*tanSpaceLobe,1).unsqueeze(1),0,100) #TODO: look for definition of dot product, can it really be negative?
            fSpecular = torch.pow(fSpecular, sprime)
            fSpecular = fSpecular * norm
            
            color = color + (albedo  * (fSpecular * self._specCoef +self._diffCoef) * fDiffuse)
            


        #do tonemapping
        color = torch.where(color > 0,color / (color + 1),color*0)
        color = torch.where(color > 0,1.0 - torch.exp(-color*self._exposure),color*0)
        color = torch.where(color > 0, torch.pow(color.abs() + 1e-10, 1/self._gamma),color*0) 
        
        mask = (texCoords.permute(0,3,1,2)[:,1:2] == -1)
        color[torch.cat((mask, mask, mask), dim = 1)] = 0
        
        return color

    def getAlignment(self):
        return 0

