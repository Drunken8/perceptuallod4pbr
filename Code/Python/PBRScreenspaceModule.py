import torch
import torch.nn as nn
import numpy as np
import torch.nn.functional as F
import math
from skimage import io

import torchvision

class ScreenSpaceShading(nn.Module):
    """
        performs screenspace shading for Frequency Domain Normal Map Filtering,
        as described in "Frequency Domain Normal Map Filtering" by Han et. al
        The goal is a set of optimised Lobe-Textures to represent the Normals
        optimal in an perceptual sense.
        As loss is a Method from "The Unreasonable Effectiveness of Deep Features as a Perceptual Metric" 
        suggested. Additional losses like maximizing difference between lobes and minimizing difference between lods
        may be needed.
    
        The only optimizable Variables should be the lobe textures. For other Methodes (PBR) it may be useful to
        optimize additional weights for material properties in case they are not easily computed analytically.
        
        _lobes will contain #noLobes optimized Textures in TangentSpace (like standart NormalMaps) with the additional 4th component
    
        Input are the textures from deferred rendering:
            -3D Texture-Coordinates (0)
            -3D EyeDirViewSpace-Directions (1)
            -3D NormalViewspace-Directions (2)
            -3D Tangentviewspace-Drictions (3)
            -3D Color-Vector (albedo) (4)
    """

    def makeLobeTexture(self, dim, device):
        return nn.Parameter(torch.cat((torch.ones((1,1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)/2, #tangent dir
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)/2, #bitangent dir
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)*0.625, #normal dir
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)/4, #alpha
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)/2, #color r
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)/2, #color g
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)/2, #color b
            torch.zeros((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False), #metallic init as none metal
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False)*0.2, #roughness init as flat
            torch.ones((1, 1,dim,dim), dtype=torch.float32, device=device, requires_grad=False) #ao init as no AO-> no shadow
            ), dim=1).requires_grad_())
    
    def getParamsAsFlatList(self):
        return [item for sublist in self._lobes for item in sublist]

    def getLobesOfLOD(self, LOD, detached = True):
        tmp = []
        for i in range(self._noLobes):
            if(detached):
                tmp.append(self._lobes[i][LOD].detach())
            else:
                tmp.append(self._lobes[i][LOD])
        return torch.cat(tmp, dim= 0)

    def __init__(self, device,albedo, dim = 512, noLobes = 4, usedBatchsize = 10, loadFromFile = False, initFromFile = False):
        super().__init__()
        self._device = device
        self._noLobes = noLobes
        self._spec = 20 #specular exponent
        self._specCoef = 0.99 #specular coefficient
        self._diffCoef = 0.01 #diffuse coefficient
        self._exposure = 1.0
        self._gamma = 2.2
        self._dim = dim
        self._albedo = torch.cat(usedBatchsize*[albedo.permute(0,3,1,2)])
        
        self._batchsize = usedBatchsize
        #setup trainable tensor with requires_grad=True in init for autograd 
        #shape is for each lobe dim*dim*3 Channels as a (normalized) direction and an additional alpha value
        #type is torch.float32 for standart 0-1 range, later represented with less bits

        self._lobes = []
        if(loadFromFile == False):
            for i in range(self._noLobes):
                dim = self._dim
                currLob = []
                while(dim >= 1):
                    if(initFromFile == False):
                        currLob.append(self.makeLobeTexture(dim, self._device))
                    else:
                        currLob.append(self.initLobeTensorFromFile(dim,self._device))
                        print("INIT LOB " + str(i) + " size " + str(dim))

                    dim = dim // 2  #do integer division
                self._lobes.append(currLob)
        else:
            self.loadLobeTensorsFromFile(math.log2(dim)+1)

        #print(len(self._lobes), len(self._lobes[0]), self._lobes[0][0].shape)
    

    def warpMIP(self, MipMap, sampleImage):
        """ 
            Samples a MipMap (List of Texture levels (decreasing detail)) according to 3D Coordinates in sampleImage (analougus to grid_sample)
            return sampled and warped image

            Mipmap has shapeof List(noLods) Texture(BxCxWXH)
            sampleImage has shapeof Texture(BxWxHxC) where C is 3 in Range [-1,1] (3. Coord is corrected to [0,1])
        """
        #first warp every level indipendent
        warpedLevels = []
        for i in range(len(MipMap)): 
            currImg = torch.cat(self._batchsize* [MipMap[i]])
            warpedImg = F.grid_sample(currImg, sampleImage[:,:,:,0:2], mode='bilinear', padding_mode='reflection', align_corners=False)
            warpedLevels.append(warpedImg)
            del warpedImg
            del currImg
        warpedTensor = torch.stack(warpedLevels,dim = 0)
        for i in reversed(range(len(warpedLevels))):
            del warpedLevels[i]
        del warpedLevels
        torch.cuda.empty_cache()
        ##torchvision.utils.save_image( warpedTensor[:,0,0:3,:,:], "warpedTensor.png")

        #make a mask to weight entries,
        correctedSampleImage = (sampleImage[:,:,:,2:].permute(0,3,1,2)  + 1) /2 #is in -1 to 1, needs to be in 0 to 1...
        w = torch.cat(warpedTensor.shape[0]*[correctedSampleImage*(len(MipMap)-1)], dim = 1)
        del sampleImage
        del correctedSampleImage
        torch.cuda.empty_cache()
        weights = []
        for i in range(len(MipMap)): 
            tmp = (w - i) #get difference
            tmp.abs_()
            tmp = 1 - tmp
            mask = (tmp < 0) | (tmp > 1)
            tmp[mask] = 0 #erase to big or to small fractions for this lod
            del mask
            weights.append(tmp)
        weightTensor = torch.stack(weights, dim = 0)
        for i in reversed(range(len(weights))):
            del weights[i]
        del weights
        torch.cuda.empty_cache()
        ##torchvision.utils.save_image( weightTensor[:,0,0:3,:,:], "weightTensor.png")
        
        #weight entries
        weightedwarpedTensor = warpedTensor*weightTensor #torch.where((weightTensor <1) &(weightTensor > 0 ),warpedTensor*weightTensor,warpedTensor*0)
        del warpedTensor
        del weightTensor
        #reduce with sum to finish interpolation
        weightedwarpedreducedTensor = weightedwarpedTensor.sum(dim=0)
        del weightedwarpedTensor
        torch.cuda.empty_cache()
        return weightedwarpedreducedTensor


    def saveLobeTensorsToFile(self):
        for i in range(len(self._lobes)):
            #save each Lobe
            for j in range(len(self._lobes[i])):
                torch.save(self._lobes[i][j], "C:\\Users\\Moritz\\Documents\\PerceptualLOD4PBR\\Code\\Python\\backup\\" + str(i) + " " + str(j) + ".pt")
        
    def loadLobeTensorsFromFile(self, noLovels = 10):
        self._lobes.clear()
        print("cleared _lobes, loading from file")
        for i in range(self._noLobes):
            currList = []
            for j in range(int(noLovels)):
                curr = torch.load( "C:\\Users\\Moritz\\Documents\\PerceptualLOD4PBR\\Code\\Python\\backup\\" + str(i) + " " + str(j) + ".pt")
                curr.requires_grad_()
                currList.append(curr)
            self._lobes.append(currList)

    def initLobeTensorFromFile(self,dimension,device):
        normals = io.imread("C:\\Users\\Moritz\\Documents\\PerceptualLOD4PBR\\Code\\vMFFrame\\assets\\Metal_Plate_040_normal.jpg")[:,:,0:3]
        albedo = io.imread("C:\\Users\\Moritz\\Documents\\PerceptualLOD4PBR\\Code\\vMFFrame\\assets\\Metal_Plate_040_basecolor.jpg")[:,:,0:3]
        metallic = io.imread("C:\\Users\\Moritz\\Documents\\PerceptualLOD4PBR\\Code\\vMFFrame\\assets\\Metal_Plate_040_metallic.jpg")[:,:,0:1]
        roughness = io.imread("C:\\Users\\Moritz\\Documents\\PerceptualLOD4PBR\\Code\\vMFFrame\\assets\\Metal_Plate_040_roughness.jpg")[:,:,0:1]
        ao = io.imread("C:\\Users\\Moritz\\Documents\\PerceptualLOD4PBR\\Code\\vMFFrame\\assets\\Metal_Plate_040_ambientOcclusion.jpg")[:,:,0:1]
        
        normals = ((torch.from_numpy(np.array(normals).astype(np.float32)).unsqueeze(0).permute(0,3,1,2).flip(2)/ 256.0  *2 -1 )*0.25 +1)/2
        albedo = torch.from_numpy(np.array(albedo).astype(np.float32)).unsqueeze(0).permute(0,3,1,2).flip(2)/ 256.0
        metallic = torch.from_numpy(np.array(metallic).astype(np.float32)).unsqueeze(0).permute(0,3,1,2).flip(2)/ 256.0
        roughness = torch.from_numpy(np.array(roughness).astype(np.float32)).unsqueeze(0).permute(0,3,1,2).flip(2)/ 256.0
        ao = torch.from_numpy(np.array(ao).astype(np.float32)).unsqueeze(0).permute(0,3,1,2).flip(2)/ 256.0
        
        dim = normals.shape[2]
        alpha = torch.ones((1, 1,dim,dim), dtype=torch.float32, requires_grad=False)/4
        
        
        lob = torch.cat([normals, alpha, albedo, metallic, roughness, ao],dim = 1)
        torchvision.utils.save_image( normals, "NormPlain.png")
        del normals
        del albedo
        del metallic
        del roughness
        del ao
        return nn.Parameter(F.interpolate(lob,size = dimension).to(self._device).requires_grad_())

    def normSumAlpha(self):
        with torch.no_grad():
            for i in range(len(self._lobes[0])): #norm each Level
                curr = self.getLobesOfLOD(i,False)[:,3:4] #collect current level
                norms = torch.sum(curr,dim=0,keepdim=True) #calculate SUM
                for j in range(self._noLobes):
                    self._lobes[j][i][:,3:4] /= norms #normalize SUM -> length will be 1
                del norms
                del curr
        

    def clampAllLobesToColor(self):
        with torch.no_grad():
            for lob in self._lobes:
                for lod in lob:
                    lod.clamp_(0,1)



    #@profile
    def forward(self, input):
        """ 
            Input are the textures from deferred rendering
            Output is the shaded image
        """
        self.clampAllLobesToColor()
        self.normSumAlpha()

        texCoords = input[:,0].permute(0,2,3,1) * 2 - 1
        V = input[:,1]
        normalViewSpace = input[:,2]
        tangentViewSpace = input[:,3]
        L = input[:,4]
        del input
        
        ##torchvision.utils.save_image( texCoords.permute(0,3,1,2), "texCoords.png")
        #albedo = F.grid_sample(self._albedo, texCoords[:,:,:,0:2], mode='bilinear', padding_mode='reflection', align_corners=False)
        
        #compute bitangent for transformation
        biTangentViewSpace = torch.cross(normalViewSpace, tangentViewSpace, dim=1)
        biTangentViewSpace = F.normalize(biTangentViewSpace, p=3, dim=1)
        mask = torch.isnan(biTangentViewSpace)
        biTangentViewSpace[mask] = 0 #erase nans
        del mask
        torch.cuda.empty_cache()

        ##torchvision.utils.save_image( (texCoords[:,:,:,2:].permute(0,3,1,2) +1 )/2, "level.png")
      

        color = torch.zeros((1,3,self._dim, self._dim), dtype=torch.float32, device=self._device, requires_grad=False)
        for i in range(self._noLobes):
            #prepare lobes
            warpedLobes = self.warpMIP(self._lobes[i], texCoords)#F.grid_sample(torch.cat(self._batchsize* [self._lobeParam[i:i+1]]), texCoords[:,:,:,0:2], mode='bilinear', padding_mode='reflection', align_corners=False)
            ##torchvision.utils.save_image( warpedLobes[:,0:3,:,:], str(i) + "warpedLobes.png")
            alpha = torch.clamp( warpedLobes[:, 3:4] ,1e-10,1.0)
            r = torch.clamp(2.0 * warpedLobes[:, 0:3] - 1.0,-1.0,1.0)
            albedo = warpedLobes[:, 4:7]
            metallic = warpedLobes[:, 7:8]
            roughness = warpedLobes[:, 8:9]
            ao = warpedLobes[:, 9:]
            del warpedLobes
            torch.cuda.empty_cache()
            torchvision.utils.save_image( (r+1)/2, "r.png")
            torchvision.utils.save_image( alpha, "alpha.png")
            torchvision.utils.save_image( albedo, "albedo.png")
            torchvision.utils.save_image( metallic, "metallic.png")
            torchvision.utils.save_image( roughness, "roughness.png")
            torchvision.utils.save_image( ao, "AO.png")

            r = r / alpha
            r[torch.isinf(r)] = 0 #erase infs for norm to work
            
            F0 = metallic * 0.04 + (1-metallic)*albedo

            lenr = torch.clamp(torch.sqrt(torch.sum(torch.mul(r, r)+1e-10, 1).unsqueeze(1)),0,0.999) #length of mu
            mu =  F.normalize(r, p=3, dim=1)
            del r

            kappa =  (3.0*lenr - lenr*lenr*lenr)/(1.0 - lenr*lenr)
            del lenr
            roughness = torch.sqrt(torch.pow(roughness,2)+1/(2*kappa))
            

            N = tangentViewSpace * mu[:,0:1] + biTangentViewSpace * mu[:,1:2] + normalViewSpace * mu[:,2:]
            N = F.normalize(N, p=3, dim=1)

            #vec3 vhalf = normalize(vToCamera + light);
            H = F.normalize(V + L)

            torchvision.utils.save_image( N, "N.png")
            torchvision.utils.save_image( roughness, "roughness2.png")
            #NDF, G, F
            NDF = self.DistributionGGX(N, H, roughness)
            G = self.GeometrySmith(N, V, L, roughness)
            Fres = self.fresnelSchlick(torch.clamp(torch.sum(N*V,1,keepdim=True),0,1), F0)
            #KS+KD
            kD = torch.clamp((1-Fres)*(1-metallic),0,1)
            torchvision.utils.save_image( NDF, "NDF.png")
            torchvision.utils.save_image( G, "G.png")
            torchvision.utils.save_image( Fres, "Fres.png")
            torchvision.utils.save_image( kD, "kD.png")
            #Numerator/ denominator = specular
            NDotL = torch.clamp(torch.sum(N*L,1,keepdim=True),1e-10,1)
            HDotL = torch.clamp(torch.sum(H*L,1,keepdim=True),1e-10,1)
            reflect = (2*HDotL*H-L)
            del HDotL
            torchvision.utils.save_image( NDotL, "NDotL.png")
            torchvision.utils.save_image( reflect, "reflect.png")
            denom = 4* torch.clamp(torch.sum(N*reflect,1,keepdim=True),1e-10,1) * NDotL
            specular = NDF*G*Fres / denom
            torchvision.utils.save_image( specular, "specular.png")
            torchvision.utils.save_image( NDF*G*Fres, "nomi.png")
            torchvision.utils.save_image( denom, "denom.png")
            
            #Accumulation
            color = color + (kD*albedo/math.pi+specular) * NDotL + albedo * ao * 0.0025
            del NDotL
            torch.cuda.empty_cache()


        #do tonemapping
        color = torch.where(color > 0,color / (color + 1),color*0)
        color = torch.where(color > 0,1.0 - torch.exp(-color*self._exposure),color*0)
        color = torch.where(color > 0, torch.pow(color.abs() + 1e-10, 1/self._gamma),color*0) 
        
        mask = (texCoords.permute(0,3,1,2)[:,1:2] == -1)
        color[torch.cat((mask, mask, mask), dim = 1)] = 0
        
        return color

    def getAlignment(self):
        totalDif = 0
        for lob in self._lobes: #calculate for each lobe
            for i in range(len(lob)-1): #difference between Lods
                currLOD = F.interpolate(lob[i], scale_factor=0.5) #scale current lod down to match lower lod
                totalDif += (currLOD-lob[i+1]).abs().mean() / len(lob) #difference is low if alignment is good, scale by numberof levels
        return totalDif / self._noLobes # scale by number of lobes

    def DistributionGGX(self, N, H, roughness):
        a2 = torch.pow(roughness, 2)
        denom = torch.pow(torch.clamp(torch.sum(N*H,1,keepdim=True),1e-10,1),2) * (a2-1.0) +1.0
        return a2/(torch.pow(denom,2)*math.pi)

    def GeometrySchlickGGX(self, NdotV, roughness):
        k = torch.pow(roughness +1,2)/8
        return NdotV/(NdotV *(1-k)+k)

    def GeometrySmith(self, N, V, L, roughness):
        NdotV = torch.clamp(torch.sum(N*V,1,keepdim=True),0,1)
        NdotL = torch.clamp(torch.sum(N*L,1,keepdim=True),0,1)
        return self.GeometrySchlickGGX(NdotV,roughness)*self.GeometrySchlickGGX(NdotL,roughness)

    def fresnelSchlick(self,cosTheta, F0):
        return F0 + (1-F0) * torch.pow(1-cosTheta, 5)