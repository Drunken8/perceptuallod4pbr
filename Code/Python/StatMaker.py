import ScreenspaceModule
import NormalModule
import DeferredDataset
import PBRScreenspaceModule
import models
import FOURPBRScreenspaceModule
import StatSet
import torch
import os
import sys
import statistics
from skimage import io
import numpy as np
import torchvision
from torch.utils.data import Dataset, DataLoader
from torch.utils.tensorboard import SummaryWriter

import argparse


if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument("statpath", help="Path to the Statset")
    parser.add_argument("bucketCount", help="number of buckets for distribution visualisation")
    parser.add_argument("postfixes",nargs='+', help="list of model postfixes which should be included for stats")
    parser.add_argument("-d", "--device", default='cuda:0', help = "Used device for optimization")
    
    args = parser.parse_args()

    device = torch.device(args.device)
    names = args.postfixes #["mipmap", "vmf","standard","preMipMappedPy","preOptimizedPy","vmfPy","naiivePerceptualPy", "naiiveMSEPy"]
    statset = StatSet.StatDataset(args.statpath, device,names = names)

    statdataloader = DataLoader(statset, batch_size=1, shuffle=False, sampler=None,
            batch_sampler=None, num_workers=0, collate_fn=None,
            pin_memory=False, drop_last=True, timeout=0,
            worker_init_fn=None)
            
    writer = {}
    for name in names:
        writer[name] = SummaryWriter(log_dir="./stats/"+name)
    
    loss_fn = models.PerceptualLoss(model='net-lin', net='alex', use_gpu=True)
    l2criterion = torch.nn.MSELoss()
    
    l2loss= {}
    perceptualLoss= {}
    loss = {}
    for name in names:
        l2loss[name]= []
        perceptualLoss[name]= []
        loss[name] = []

    with torch.no_grad():
        print("EVALUATING")
        for t, sample_batched in enumerate(statdataloader):
            if(t%25 == 0):
                print(t/len(statset)*100, "%")
            x = sample_batched
            y_pred = x["groundtruth"][:,0]
            for name in names:
                y = x[name][:,0]

                l2 = l2criterion(y, y_pred)
                perception = loss_fn.forward(y_pred, y, normalize=True).sum() 

                l2loss[name].append(l2.item())
                perceptualLoss[name].append(perception.item())
                loss[name].append((perception + l2*100).item())
                        
        print("FINISHED EVAL")

    l1 = []
    l2 = []
    l3 = []
    l2dists = {}
    perceptdists = {}
    lossdists = {}
    for name in names:
        l1.append(max(l2loss[name]))
        l2.append(max(perceptualLoss[name]))
        l3.append(max(loss[name]))
        print(name, statistics.median(l2loss[name]),statistics.median(perceptualLoss[name]),statistics.median(loss[name]))

    numbuckets = args.bucketCount -1
    l2bucketsize = max(l1) / numbuckets
    perbucketsize = max(l2) / numbuckets
    lobucketsize = max(l3) / numbuckets
    print("l2bucketsize",l2bucketsize)
    print("perbucketsize",perbucketsize)
    print("lobucketsize",lobucketsize)
    for name in names:
        l2dists[name] = [0]*(numbuckets+1)
        perceptdists[name] = [0]*(numbuckets+1)
        lossdists[name] = [0]*(numbuckets+1)
        for sample in l2loss[name]:
            l2dists[name][int(sample/l2bucketsize)] = l2dists[name][int(sample/l2bucketsize)] +1
        for bucket in range(len(l2dists[name])):
            writer[name].add_scalar("Distributions/"  + "/l2dist",l2dists[name][bucket],global_step=bucket)
        print(name, "l2", "mean",statistics.mean(l2loss[name]))
        print(name, "l2", "variance",statistics.variance(l2loss[name]))
        print(name, "l2", "median",statistics.median(l2loss[name]))
        for sample in perceptualLoss[name]:
            perceptdists[name][int(sample/perbucketsize)] = perceptdists[name][int(sample/perbucketsize)] +1
        for bucket in range(len(perceptdists[name])):
            writer[name].add_scalar("Distributions/"  + "/perceptdists",perceptdists[name][bucket],global_step=bucket)
        print(name, "perceptual", "mean",statistics.mean(perceptualLoss[name]))
        print(name, "perceptual", "variance",statistics.variance(perceptualLoss[name]))
        print(name, "perceptual", "median",statistics.median(perceptualLoss[name]))
        for sample in loss[name]:
            lossdists[name][int(sample/lobucketsize)] = lossdists[name][int(sample/lobucketsize)] +1
        for bucket in range(len(lossdists[name])):
            writer[name].add_scalar("Distributions/"  + "/lossdists",lossdists[name][bucket],global_step=bucket)
        print(name, "loss", "mean",statistics.mean(loss[name]))
        print(name, "loss", "variance",statistics.variance(loss[name]))
        print(name, "loss", "median",statistics.median(loss[name]))