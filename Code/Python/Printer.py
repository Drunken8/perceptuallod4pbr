import ScreenspaceModule
import NormalModule
import DeferredDataset
import PBRScreenspaceModule
import models
import FOURPBRScreenspaceModule
import torch
import os
import sys
from skimage import io
import numpy as np
import torchvision
from torch.utils.data import Dataset, DataLoader
from torch.utils.tensorboard import SummaryWriter

import argparse


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--albedoPath",default="./Code/vMFFrame/assets/tst_albedo.png", help="Path to the albedo Texture")
    parser.add_argument("evalPath", help="Path to the evaluationSet")
    parser.add_argument("-out", "--outputPath",default="./Code/vMFFrame/vc2015/StatSet/", help="Path where images will be saved")
    parser.add_argument("-outName", "--outputPostfix",default="default", help="Postfix of images (modelname)")
    parser.add_argument("-b", "--batchsize",type=int,  default=1, help="Used batchsize")
    parser.add_argument("-d", "--device", default='cuda:0', help = "Used device for optimization")
    
    parser.add_argument("-lff", "--loadFromFile", type=bool,default=False, help = "wether to load model from file. Priority is lff>iff>sff")
    parser.add_argument("-iff", "--initFromFile", type=bool,default=False, help = "wether to init model with MIP approach from file. Priority is lff>iff>sff")
    parser.add_argument("-sff", "--setFromFile", type=bool,default=False, help = "wether to set model from precomputed textures. Priority is lff>iff>sff")
    
    parser.add_argument("-n", "--runName", type=str,default="default", help = "The name of the run, used to store different models")
    parser.add_argument("-nID", "--runID", type=int,default=0, help = "The ID of the run to load, only used if lff==true")
    
    args = parser.parse_args()


    device = args.device
    albedo = io.imread(args.albedoPath)[:,:,0:3] / 255.0
    albedo = torch.from_numpy(np.array(albedo).astype(np.float32)).unsqueeze(0).to(device)
    albedo = torch.flip(albedo, (1,2)) #flip image-y because texcoords are in other system
    
    batch_size = args.batchsize
    model = FOURPBRScreenspaceModule.ScreenSpaceShading(device, albedo, usedBatchsize = batch_size,loadFromFile = False,initFromFile = False, setfromfile = False,runname = args.runName,runID = args.runID)
    evalSet = DeferredDataset.DeferredDataset(args.evalPath, device)
    
    evaldataloader = DataLoader(evalSet, batch_size=batch_size, shuffle=False, sampler=None,
            batch_sampler=None, num_workers=0, collate_fn=None,
            pin_memory=False, drop_last=True, timeout=0,
            worker_init_fn=None) 
            
    
    with torch.no_grad():
        
        print("EVALUATING")
        for t, sample_batched in enumerate(evaldataloader):
            print(str(t/(len(evalSet))*100) + "%")
            x = sample_batched
            y_pred = model(x[:,1:])
            y = x[:,0]
            torchvision.utils.save_image( y_pred,args.outputPath + str(evalSet.getSeed(t)) + args.outputPostfix)
