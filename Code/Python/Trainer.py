import ScreenspaceModule
import NormalModule
import DeferredDataset
import PBRScreenspaceModule
import models
import FOURPBRScreenspaceModule
import torch
import os
import sys
from skimage import io
import numpy as np
import torchvision
from torch.utils.data import Dataset, DataLoader
from torch.utils.tensorboard import SummaryWriter

import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--albedoPath",default="./Code/vMFFrame/assets/tst_albedo.png", help="Path to the albedo Texture")
    parser.add_argument("dataPath", help="Path to the dataSet")
    parser.add_argument("evalPath", help="Path to the evaluationSet")
    parser.add_argument("-b", "--batchsize",type=int,  default=4, help="Used batchsize")
    parser.add_argument("-ba", "--batchAccumulate",type=int,  default=15, help="Number of Btaches to accumulate gradient ba*b=effective batch size")
    parser.add_argument("-lr", "--learningrate",type=float,  default=0.005, help="Used learnigrate")
    parser.add_argument("-d", "--device", default='cuda:0', help = "Used device for optimization")
    parser.add_argument("-maxE", "--maximalEpochs", type=int,default=8, help = "Maximum Epochs to run")
    parser.add_argument("-bf", "--backupFrequency", type=int,default=200, help = "Do a backup every bf batches")
    
    parser.add_argument("-lff", "--loadFromFile", type=bool,default=False, help = "wether to load model from file. Priority is lff>iff>sff")
    parser.add_argument("-iff", "--initFromFile", type=bool,default=False, help = "wether to init model with MIP approach from file. Priority is lff>iff>sff")
    parser.add_argument("-sff", "--setFromFile", type=bool,default=False, help = "wether to set model from precomputed textures. Priority is lff>iff>sff")
    
    parser.add_argument("-n", "--runName", type=str,default="default", help = "The name of the run, used to store different models")
    parser.add_argument("-nID", "--runID", type=int,default=0, help = "The ID of the run to load, only used if lff==true")
    
    args = parser.parse_args()


    #model preparation
    device = torch.device(args.device)
    albedo = io.imread(args.albedoPath)[:,:,0:3] / 255.0
    albedo = torch.from_numpy(np.array(albedo).astype(np.float32)).unsqueeze(0).to(device)
    albedo = torch.flip(albedo, (1,2)) #flip image-y because texcoords are in other system
    batch_size = args.batchsize
    model = FOURPBRScreenspaceModule.ScreenSpaceShading(device, albedo, usedBatchsize = batch_size,loadFromFile = False,initFromFile = False, setfromfile = False,runname = args.runName,runID = args.runID)
    #model = PBRScreenspaceModule.ScreenSpaceShading(device, albedo, usedBatchsize = batch_size,loadFromFile = False,initFromFile = True)
    #model = ScreenspaceModule.ScreenSpaceShading(device, albedo, usedBatchsize = batch_size,loadFromFile = False)
    #model = NormalModule.ScreenSpaceNormalShading(device, albedo, usedBatchsize = batch_size,loadFromFile = True)
    
    #Datasets
    dataset = DeferredDataset.DeferredDataset(args.dataPath, device)
    evalSet = DeferredDataset.DeferredDataset(args.evalSet, device)
    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False, sampler=None,
            batch_sampler=None, num_workers=0, collate_fn=None,
            pin_memory=False, drop_last=True, timeout=0,
            worker_init_fn=None)
    evaldataloader = DataLoader(evalSet, batch_size=batch_size*2, shuffle=False, sampler=None,
            batch_sampler=None, num_workers=0, collate_fn=None,
            pin_memory=False, drop_last=True, timeout=0,
            worker_init_fn=None) 

    #optimizer  
    learningRate = args.learningrate
    loss_fn = models.PerceptualLoss(model='net-lin', net='alex', use_gpu=True)
    optimizer = torch.optim.Adam(model.getParamsAsFlatList(), lr=learningRate, betas=(0.9, 0.999))
    l2criterion = torch.nn.MSELoss()

    #Summary Writer Setup
    writer = SummaryWriter()
    writer.add_scalar("Setup\Learning rate", learningRate)
    writer.add_scalar("Setup\Batchsize", batch_size)
    
    for i in range(args.maximalEpochs):
        model.outputLobesToFiles("OUTPUT/EPOCH" + str(i))
        print("OUTPUTTED")
        #evaluate on EvalSet
        with torch.no_grad():
            l2loss=0
            perceptualLoss=0
            loss = 0
            print("EVALUATING")
            for t, sample_batched in enumerate(evaldataloader):
                x = sample_batched
                y_pred = model(x[:,1:])
                y = x[:,0]
                del x
                l2loss += l2criterion(y, y_pred)
                perceptualLoss += loss_fn.forward(y_pred, y, normalize=True).sum() 
                loss += perceptualLoss + l2loss*100 + model.getAlignment()/10
            writer.add_scalar("Eval/Perceptual Loss (per Sammple)",perceptualLoss.item(),global_step=(i)*(len(dataset)))
            writer.add_scalar("Eval/MSE Loss (per Sammple)",l2loss.item(),global_step=(i)*(len(dataset)))
            writer.add_scalar("Eval/loss (per Sammple)",loss.item(),global_step=(i)*(len(dataset)))
            writer.add_scalar("Eval/Alignment",model.getAlignment().item(),global_step=(i)*(len(dataset)))
            print("FINISHED EVAL")
            del l2loss,perceptualLoss,loss

        #Do one Epoch on Dataset
        for t, sample_batched in enumerate(dataloader):
            torch.cuda.empty_cache()
            # aquire sample
            x = sample_batched
            # Forward pass: Compute predicted y by passing x to the model
            y_pred = model(x[:,1:])
            y = x[:,0]
            del x

            # Compute and print loss
            l2loss = torch.nn.MSELoss()
            perceptualLoss = torch.mean(loss_fn.forward(y_pred, y, normalize=True))
            loss = perceptualLoss #possibly compbine losses

            #record strats
            print(i,t*batch_size, loss.item()/batch_size)
            writer.add_scalar("Losses/Perceptual Loss (per Sammple)",perceptualLoss.item()/batch_size,global_step=(i)*(len(dataset)/batch_size)+t)
            writer.add_scalar("Losses/MSE Loss (per Sammple)",l2loss(y, y_pred).item()/batch_size,global_step=(i)*(len(dataset)/batch_size)+t)
            writer.add_scalar("Losses/loss (per Sammple)",loss.item()/batch_size,global_step=(i)*(len(dataset)/batch_size)+t)
            writer.add_scalar("Losses/Alignment",model.getAlignment().item(),global_step=(i)*(len(dataset)/batch_size)+t)

            #accumulate gradient
            loss.backward()
            
            #apply gradients every couple of batches to archieve bigger effective batchsize
            if((t+1)%args.batchAccumulate == 0):
                print("STEP, akkumulating Gradient again")
                optimizer.step()
                optimizer.zero_grad() #delete gradients after using them

            if t % args.backupFrequency == 0:
                torchvision.utils.save_image( y_pred, str(t) +"pred.png")
                torchvision.utils.save_image( y, str(t) +"gt.png")
                model.saveLobeTensorsToFile(runID = int((i)*(len(dataset)/batch_size)+t)) #backup
            
            del l2loss, perceptualLoss,loss,y, y_pred