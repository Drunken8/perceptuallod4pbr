# Perceptual Level-of-Detail for physically based rendering

This repository contains material for the Master Thesis "Perceptual Level-of-Detail for physically based rendering".
A digital copy can be found in [Documents](Documents/). All used Figures and .tex files are also included.
All necessary code is in the corresponding named folder. The Models and teir summaries are also included.
Summaries are in the [Runs](runs/) folder, the models themself in the folder [Python](Code/Python/). The summaries can be viewed with tensorboard.

## Third party applictaions

### Prerequiresits

To fully facilitate this repository these third party applications are needed:

- PyTorch 1.3.1+cu92
- Tensorboard 2.0.0
- Visual Studio 2017

Additionally access to a high performance GPU is recommended.

### Integrated third party applications

- [Perceptual Similarity Loss](Code/Python/models) ([GIT](https://github.com/richzhang/PerceptualSimilarity))
- [Cinder 0.9.1](www.libcinder.org) as framework for rendering

## How to use the included applications: 

### Datageneration:
Entry point is [vMFFrame.sln](/Code/vMFFrame/vc2015/vMFFrame.sln) or [vMFFrameApp.cpp](/Code/vMFFrame/src/vMFFrameApp.cpp). Use for compilation Visual Studio 2017.

Configure application with [Settings.xml](Code/vMFFrame/assets/settings.xml).
Main configuration point is the RenderMode:

- VMF: Use VMF based on Phong
- MIP: Use Phong with mipmapping
- SIMPLE: Use Phong without mipmapping
- CTS: Use Cook-Torrance-Sparrow Model
- CTSVMF: Use Cook-Torrance-Sparrow Model with VMF representation
- SUPERSAMPLE: CTS but Supersampled
- DEFERRED: Draw scene to GBuffers and output to file
- DATAGEN: Generate Dataset based on given name, seed and size

### Training:
Use for Python scripts this directory as root.

Entry point for Training is [Training.py](Code/Python/Training.py).
Prereqiresits: Full DataSet+EvaluationSet, possibly acess to material maps.
To start Training refer to Training.py s help page (python Training.py -h), minimum input is paths to datasets

### Evaluation:
Use for Python scripts this directory as root.

For evaluation a evaluation dataset and trained models (or use [intermediate](Code/Python) results) or already rendered predictions are necessary.
Use [Printer.py](Code/Python/Printer.py) to render predictions of trained models for an evaluation set.
Use [Statmaker.py](Code/Python/Statmaker.py) to evaluate rendered predictions and generate distributions. Results can be viewed with Tensorboard.
Refer to help pages of scripts for help with input.

```
NOTE: For other sources refer to the included digital version of the thesis!
```