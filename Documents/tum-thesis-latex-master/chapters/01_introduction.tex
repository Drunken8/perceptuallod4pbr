% !TeX root = ../main.tex
% Add the above to each chapter to make compiling the PDF easier in some editors.

\chapter{Introduction}\label{chapter:introduction}

\section{Level of Detail and Normal Mapping}
Level of Detail (LOD) is a much used concept in computer graphics of reducing the amount of information to render for objects less important to the scene. One of the main goals for LOD is the reduction of workload on the rendering pipeline. Many methods that use this concept not only reduce computation time but also improve quality. The reduced information can be the vertex count of a mesh, the size of a texture, the quality of the used shading solution or the resolution of a physics simulation. For determining the importance of an object in the scene there are multiple metrics available and even the paper handled as the origin of LOD \cite{10.1145/563274.563323} mentions at least three: visibility, distance and movement speed.\\ \\
There is a multitude of established methods that facilitate the concept of LOD.
\subsubsection{Mesh LOD} The basic idea of LOD for meshes is to have multiple versions of the same mesh. If the mesh is rendered in greater distance a version with less detail is used. For meshes a basic method is  discrete LOD: Each sub-mesh is precomputed with a basic algorithm like edge-collapse, the mesh is then replaced on discrete predefined distances. The computational gain is archived with the reduction of vertices present in the scene if some objects are further away from the viewer. While this can lead to unpleasant pop in effects, it can significantly reduce computation time for meshes with a very high poly count. The popping effect can partially avoided by using more sophisticated methods for sub-mesh generation or a less naive blending of LODs. Mesh LOD have their biggest impact if many high poly meshes are rendered on the same time. A classic example for high poly count meshes that appear in crowds are characters.
\newpage
\subsubsection{Texture LOD} MIPMapping is the goto-solution for textures. The data structure was initially described by W. Lance \cite{10.1145/800059.801126} and is mainly used for textures, although the design is not limited to image data. 
MIPMaps are a pyramid of textures where each level has a quarter of the previous size. Each Level is a smaller, prefiltered version of the original image. The final value is acquired by applying trilinear filtering. First the texels are interpolated bilinearly in levels, and then linearly between the already interpolated levels.
As described in the original paper, comes the effectiveness of MipMaps from the partial pre-computation of interpolations and the later approximation of the remaining interpolation between levels, where only computational cheap linear interpolation is used. \\

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{figures/mipmapStorage.png}
\caption{A MIPMap in memory. Channels are stored separately (RGB). 3D-Coordinate system is overlayed. Illustrations from original paper \cite{10.1145/800059.801126}.}
\label{mipmemory}
\end{figure}

Fitting for the name MipMapping (acronym MIP from the Latin "multum in parvo" meaning "many things in a small place") the memory usage only increases by one third. This fact is shown graphically in Figure \ref{mipmemory}. Only a single monochrome pixel is unused. This is a comparatively small downside for the decrease in computational effort needed and image quality gained. Without sufficient filtering of textures for distant objects some artefacts like flickering induced by aliasing (Figure \ref{mipwik}) or Moire-Effects may occur.\\



\begin{figure}
\centering
\includegraphics[width=1.0\textwidth]{figures/Mipmap_Aliasing_Comparison.png}
\caption{MipMapping Aliasing Comparision. Image from Wikipedia \cite{mipwiki} }
\label{mipwik}
\end{figure}

\newpage
Other artefacts mainly occur in combination with movement. One of the visually most striking is a flickering effect on colour or specular highlights.
Normally the prefiltering of images comes with some sort of averaging of the original pixels (except if nearest neighbour interpolation is used). One of the simplest ways of interpolation is linear interpolation, resulting in equally weighted averaging of finer texels.
\subsubsection{Physics simulation LOD}
Physical simulations are a computational expensive task, especially if done on interactive time scales. Changing the resolution in less important areas is a reasonable method to reduce unnecessary computation time. They can profit immensely if LOD techniques are applied. \\
Barbara Solenthaler and Markus Gross \cite{10.1145/2010324.1964976} for example split fluid simulation into two detail stages with complex flows and less complex flows while having mechanisms in place for taking the viewer position into account. Focusing on the viewer for optimization might lead to inaccurate, yet still believable results which is adequate for many interactive applications.\\
There are also methods for simplifying grids for physical simulations for clothes \cite{clothLOD} or fluid surface simulation. Outfits and cloth for example are often only simulated at very close view distance (e.g. the main character of a game) and animated from prebaked simulations in other regions.
%what is LOD,general concept \\
%MIPMapping and other specific methodes why?
%Maybe use explanation of a book 

\newpage
\section{Problems of the standard MIP approach}
For most use cases MipMapping is a very good solution. Textures containing linear interpolatable information like scalar material attributes or independent vectors like colour information work very well and can benefit from the interpolation strategy.\\ \\
This situation changes however for information where the lighting equation is not linear.
For example if the standard MIP method is used for normals the results differ strongly from a non-filtered rendering. If you consider noise as an arbitrary normal map and the generation of new levels as an averaging operation, it is easy to imagine the result in higher levels as flat. 

\begin{figure}[h!]
\centering
\includegraphics[width=0.4\textwidth]{figures/noise.png}
\includegraphics[width=0.4\textwidth]{figures/noise16x16BIG.png}
\caption{Illustration of averaging effects. Left: original (512x512), Right: downsampled equal to 16x16 MipMapLevel and scaled back up for comparison. \label{noisecomp}}
\end{figure}

Figure \ref{noisecomp} illustrates this effect on a scalar version, showing a reduction in value range. The noise can be interpreted as variations from an upright normal (50\% grey) in negative (0\% grey) and positive (100\% grey) x-direction. The scaled version shows almost no variations left. That is however not what we see in reality. If we examine a bumpy surface from a distance, we can still notice roughness although we might not recognize details \cite{antialiasingBumpMaps}.
 In physically based rendering this would result in a less rough looking surface than it is based on the detail of the normal map (Figure \ref{roughnesstrans}). 
This problem can also hold for other material attributes, especially those with high frequency changes. A material can look homogeneous metallic from great distance if there exist enough sharp, completely metallic, spots distributed over its surface.\\

 \begin{figure}[h!]
 \centering
\includegraphics[width=1\textwidth]{figures/roughnessEx.png}
\caption{Illustration of detail transferring to roughness at greater distances. Image from \cite{10.1145/1275808.1276412}.}
\label{roughnesstrans}
 \end{figure}
%explain normal approach \\
%https://web.archive.org/web/20140414134825if_/http://staff.cs.psu.ac.th/iew/cs344-481/p1-williams.pdf
%explain resulting problems for Normals \\
%eg. flat surface and aliasing 

\section{Human perception and computer vision}

Perception is a complex task, and as such it is often studied to find mechanisms responsible for results we get. 
%explain how human and computer vision works and differs, raw pixeldata vs eg shapes\\
\subsection{Human perception}
Visual perception is one of the senses of humans, the sense of sight. Evolution tweaked the Human Visual System (HVS) for fast identification and decision making to recognize possible dangers. As such it is heavily influenced by past experiences, we make in everyday life. It is for example generally expected to view objects from a, for us, normal perspective and have the sun as the strongest light source from above.\\
There are some effects that are shared equally between all humans because they are a product of the function of our brain, not product of experience. They are formulated under the principles of grouping in the field of Gestalt psychology.
These laws are, as formulated by M. Wertheimer \cite{Gestalt}:
\paragraph{Law of Proximity.} Elements with small distances between them are perceived as a group.
\paragraph{Law of Similarity.} Elements that look similar are more likely to be grouped than different looking ones. This is however limited by some limits of the HVS, for example contrast sensitivity.
\paragraph{Law of Closure.} Elements resembling a closed structure are more likely to be perceived as one.
\paragraph{Law of Symmetry.} Mirrored objects are seen as the same object, likely based on our experience with reflections.
\paragraph{Law of Common Fate.} Elements moving in unity are perceived as being connected.
\paragraph{Law of Continuity.} Hidden parts of objects are seen as one. Imagine for example two differently coloured crossing lines, each line is seen as an object although there are tree parts: The obstructing line and two parts of the other line. \\

These rules apply more to grouping of elements into an object, but can lead to some interesting insights for other applications. Perceptually it makes a huge difference if a shadow is separated by illuminated pixels or not, being recognized as one shadow or multiple. Each of above rules can be transformed to a case in image comparison where a small change could make huge differences.\\

The last class of effects are physical limitations of the eye which can lead to unintuitive results.
None the less has our understanding of the HVS improved since first remarkable works in the 60s. \\
Campbell and Robson \cite{fourier} for example introduced Figure \ref{campbell} to visualize some effects of the limits of the HVS. It shows our sensitivity to the combination of frequency and contrast. It is also interesting that your perception limits may change if the image is rotated by 90 degree.\\
\begin{figure}
\centering
\includegraphics[width=0.66\textwidth]{figures/Campbell.png}
\caption{Campbell and Robson chart. Version from \cite{doi:10.1111/cgf.12001}. \label{campbell}}
\end{figure}
Another group of effects is visual masking, where the influence of two visual stimuli together is smaller than the sum of both separately. Examples include simultaneous change in contrast and overall brightness or two overlapping distortions. \\
The effect of visual saliency describes the task dependent nature of looking on objects and what structures draw more attention. After giving test persons different examination tasks for a painting and recording their eye movements, Yarbus \cite{Jarbus} described the visual path as a chain of fixations on key points of interests for solving the problem. 


\begin{figure}
\centering
\includegraphics[width=1\textwidth]{figures/paths.png}
\caption{Erperiments performed by Jarbus \cite{Jarbus} on how different examination tasks change eye movements. Illustration from M. Corsini et al.  \cite{doi:10.1111/cgf.12001}. \label{jab}}
\end{figure}

Figure \ref{jab} shows some example paths with different tasks given. Most interesting is the free exploration task as the produced path differs wildly. Giving more detail in areas focused by many tasks might help improve quality in compression tasks. \\
Colour perception may be the hardest concept to grasp. Being dependent on viewing conditions such as lighting, contrasting background or even screen settings it can produce very specific and hard to model situations. If the conditions are right it can even lead to a viral discussion, as happened under "\#Thedress", giving a good example for color perception. The image in question is easily found online and is perceived by some people as a black and blue dress while others perceive it as white and golden. Note that the image always contains the same information and is only differently perceived due to presentation conditions. 
\subsection{Computer vision}
Computers see images as data, and as such every decision made on this data is at its core a calculation. Decisions are made based on Pixels and their correlation to direct or indirect neighbours. More complex computer vision algorithms extract higher level information from pixel data, such as edges or labels for objects. Such algorithms are normally very specialized to archive best results in a single domain. \\
Due to the nature of data driven detail are computers very good in detecting very fine visual difference. They are not limited to the physical limitations the HVS has.
\subsection{Exploiting human visual perception}
Most image generation is made for humans, so in the end it is only important if we can detect differences. During rendering we can trade accuracy in areas the HVS 
is less sensitive to, for speed or more accuracy in other areas. The data in the background may differ wildly if compared via Mean Squared Error (MSE) but can be perceived as nearly the same by a viewer or vice versa. MSE in special is know to be nearly unaffected by blur while humans notice it as visually disturbing \cite{DBLP:journals/corr/abs-1801-03924}.
\section{Combination of human perception with sophisticated representations for Normals}
The main reason why MIPMapping is not optimal for normal maps is their inability to be properly linearly interpolated. I will use a different representation for normal data than vectors to work around this problem. To archive optimal results the representation will be tweaked with the HVS in mind by using a loss function that mimics its behaviour. This perceptual approach may give enough room for optimization in an otherwise mathematical optimized environment due to the special features of the HVS explained earlier.\\
Most of the work will be done in precomputation steps making the time needed for optimization less important. The focus is on the best possible end result while archiving the goals of the standard approach, making even small improvements with large time expenditure acceptable.