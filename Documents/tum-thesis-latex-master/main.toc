\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {american}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Acknowledgments}{iii}{Doc-Start}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Abstract}{iv}{chapter*.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Level of Detail and Normal Mapping}{1}{section.1.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Mesh LOD}{1}{section*.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Texture LOD}{2}{section*.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Physics simulation LOD}{3}{section*.7}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Problems of the standard MIP approach}{4}{section.1.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Human perception and computer vision}{5}{section.1.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.1}Human perception}{5}{subsection.1.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Law of Proximity.}{5}{section*.10}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Law of Similarity.}{5}{section*.11}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Law of Closure.}{5}{section*.12}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Law of Symmetry.}{6}{section*.13}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Law of Common Fate.}{6}{section*.14}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Law of Continuity.}{6}{section*.15}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.2}Computer vision}{8}{subsection.1.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.3}Exploiting human visual perception}{8}{subsection.1.3.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Combination of human perception with sophisticated representations for Normals}{8}{section.1.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Related Work}{9}{chapter.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Fixing Normal Map Filtering}{9}{section.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Imitating human perception}{10}{section.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Using perceptual notions}{11}{section.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Frequency Domain Normal Map Filtering}{12}{chapter.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Background}{12}{section.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Implementation}{16}{section.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Difference in implementations}{17}{subsection.3.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Evaluation}{19}{section.3.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Comparison methods}{19}{subsection.3.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Comparison}{20}{subsection.3.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Perceptual Level of Detail}{25}{chapter.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Perceptual Loss}{25}{section.4.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Use cases}{26}{section*.22}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Use of the perceptual loss}{26}{section*.23}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Texture optimization}{26}{section.4.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Model}{27}{subsection.4.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Deferred Shading}{27}{subsection.4.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Differentiable MipMapping}{30}{subsection.4.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.4}Optimization}{31}{subsection.4.2.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Setup}{31}{section*.27}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Data generation}{33}{section*.29}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Training}{34}{section*.31}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.5}Comparison of alternatives}{35}{subsection.4.2.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Evaluation}{37}{chapter.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Statistical Evaluation}{37}{section.5.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Case Studies}{39}{section.5.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Comparison between EM and Training}{39}{subsection.5.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}Comparison between initialisation techniques}{41}{subsection.5.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.3}Comparison between best candidates}{43}{subsection.5.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Conclusion and Future Work}{45}{chapter.6}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Conclusion}{45}{section.6.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline What has been archived?}{45}{section*.39}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline What are remaining problems?}{45}{section*.40}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Future Work}{46}{section.6.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline List of Figures}{47}{chapter*.41}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline List of Tables}{49}{chapter*.42}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Bibliography}{50}{chapter*.43}% 
